package org.ithirahad.bastioninitiative.gui;

public class BIGUIInstanceContainer { //BI GUI, not Big UI! :P
    public static AegisSystemSimpleInput temporaryAegisSystemDialog;

    public static DisruptionProgressHUDBar disruptionDatalinkBarHUD;

    public static AegisSystemManagementPanel aegisSystemPanel;
    public static AegisSystemManagementControlManager aegisSystemUIControlManager;
}
