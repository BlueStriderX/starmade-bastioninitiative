package org.ithirahad.bastioninitiative.gui;

import api.utils.gui.GUIControlManager;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.game.client.data.GameClientState;

import static org.ithirahad.bastioninitiative.gui.BIGUIInstanceContainer.aegisSystemPanel;

public class AegisSystemManagementControlManager extends GUIControlManager {
    public AegisSystemManagementControlManager(GameClientState clientState) {
        super(clientState);
    }

    @Override
    public AegisSystemManagementPanel createMenuPanel() {
        return aegisSystemPanel;
    }

    @Override
    public AegisSystemManagementPanel getMenuPanel() {
        return aegisSystemPanel;
    }

    public void activateWithInfo(String subsystemInfoText, long recalibrationStart, long recalibrationDuration, boolean isDisrupted, long timeChangeCooldownRemaining, long queuedTimeChange, int aegisSystemActivationCost, int aegisSystemDailyCost, double aegisChargeStored, int aegisChargeCapacity, long fuelledUntil, VirtualAegisSystem.AegisSystemStatus status) {
        aegisSystemPanel.updateInfo(new VirtualAegisSystem.AegisSystemStats(subsystemInfoText, recalibrationStart, recalibrationDuration, isDisrupted, timeChangeCooldownRemaining, queuedTimeChange, aegisSystemActivationCost, aegisSystemDailyCost, aegisChargeStored, aegisChargeCapacity, fuelledUntil), status);
        aegisSystemPanel.onInit();
        setActive(true);
    }
}
