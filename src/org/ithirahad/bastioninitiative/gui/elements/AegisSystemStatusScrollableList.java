package org.ithirahad.bastioninitiative.gui.elements;

import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.common.util.StringTools;
import org.schema.game.common.data.physics.Pair;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUITextOverlayTable;
import org.schema.schine.graphicsengine.forms.gui.newgui.ScrollableTableList;
import org.schema.schine.input.InputState;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;

/**
 * [Description]
 *
 * @author TheDerpGamer (MrGoose#0027)
 */
public class AegisSystemStatusScrollableList extends ScrollableTableList<Pair<String>> {

	private final VirtualAegisSystem.AegisSystemStats systemStats;
	private final VirtualAegisSystem.AegisSystemStatus status;

	public AegisSystemStatusScrollableList(InputState inputState, GUIElement guiElement, VirtualAegisSystem.AegisSystemStats systemStats, VirtualAegisSystem.AegisSystemStatus status) {
		super(inputState, guiElement.getWidth(), guiElement.getHeight(), guiElement);
		this.systemStats = systemStats;
		this.status = status;
	}

	@Override
	protected ArrayList<Pair<String>> getElementList() {
		ArrayList<Pair<String>> elementList = new ArrayList<>();
		//Don't know how many zeros you actually want or need after each number, I'm just going based off the diagram.
		elementList.add(new Pair<>("AEGIS CHARGE COST TO BRING ONLINE: ", String.valueOf(StringTools.formatPointZeroZeroZero(systemStats.getAegisSystemActivationCost()))));
		elementList.add(new Pair<>("AEGIS CHARGE CONSUMED PER DAY: ", String.valueOf(StringTools.formatPointZero(systemStats.getAegisSystemDailyCost()))));
		elementList.add(new Pair<>("STORED AEGIS CHARGE: ", StringTools.formatPointZeroZero(systemStats.getAegisChargeStored()) + " (" + StringTools.formatTimeFromMS((long) (systemStats.getAegisChargeStored() / systemStats.getAegisSystemDailyCost())) + ")"));
		elementList.add(new Pair<>("STATUS: ", status.getNiceName().toUpperCase()));
		return elementList;
	}

	@Override
	public void initColumns() {
		addColumn("", 12.0f, new Comparator<Pair<String>>() {
			@Override
			public int compare(Pair<String> o1, Pair<String> o2) {
				return 0;
			}
		});

		addColumn("", 7.5f, new Comparator<Pair<String>>() {
			@Override
			public int compare(Pair<String> o1, Pair<String> o2) {
				return 0;
			}
		});

		activeSortColumnIndex = 0;
	}


	@Override
	public void updateListEntries(GUIElementList guiElementList, Set<Pair<String>> set) {
		guiElementList.deleteObservers();
		guiElementList.addObserver(this);
		int index = 0;
		for(Pair<String> pair : set) {
			if(index <= 1) {
				GUITextOverlayTable keyTable = new GUITextOverlayTable(10, 10, getState());
				keyTable.setTextSimple(pair.a);
				GUIClippedRow keyRow = new GUIClippedRow(getState());
				keyRow.attach(keyTable);

				GUITextOverlayTable valueTable = new GUITextOverlayTable(10, 10, getState());
				valueTable.setTextSimple(pair.b);
				GUIClippedRow valueRow = new GUIClippedRow(getState());
				valueRow.attach(valueTable);

				AegisSystemStatusScrollableListRow row = new AegisSystemStatusScrollableListRow(getState(), pair, keyRow, valueRow);
				row.onInit();
				guiElementList.add(row);
			}
			index ++;
		}
		guiElementList.updateDim();
	}

	public class AegisSystemStatusScrollableListRow extends ScrollableTableList<Pair<String>>.Row {

		public AegisSystemStatusScrollableListRow(InputState inputState, Pair<String> pair, GUIElement... guiElements) {
			super(inputState, pair, guiElements);
		}
	}
}
