package org.ithirahad.bastioninitiative.gui.elements;

import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIProgressBarDynamic;
import org.schema.schine.input.InputState;

public class AegisRecalibrationElement extends GUIProgressBarDynamic {

	private VirtualAegisSystem.AegisSystemStats stats;
	private VirtualAegisSystem.AegisSystemStatus status;

	public AegisRecalibrationElement(InputState inputState, int width, int height, VirtualAegisSystem.AegisSystemStats stats, VirtualAegisSystem.AegisSystemStatus status) {
		super(inputState, width, height);
		this.stats = stats;
		this.status = status;
	}

	@Override
	public FontLibrary.FontSize getFontSize() {
		return FontLibrary.FontSize.MEDIUM;
	}

	@Override
	public String getLabelText() {
		return "A";
	}

	@Override
	public float getValue() {
		return 1;
	}
}
