package org.ithirahad.bastioninitiative.gui;

import api.utils.gui.GUIMenuPanel;
import org.ithirahad.bastioninitiative.gui.elements.AegisRecalibrationElement;
import org.ithirahad.bastioninitiative.gui.elements.AegisSystemStatusScrollableList;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.newdawn.slick.Color;
import org.schema.common.util.StringTools;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.input.InputState;

public class AegisSystemManagementPanel extends GUIMenuPanel {

	private int windowWidth;
	private int windowHeight;

	private VirtualAegisSystem.AegisSystemStats systemStats;
	private VirtualAegisSystem.AegisSystemStatus status;

	public AegisSystemManagementPanel(InputState inputState) {
		super(inputState, "AegisSystemManagementPanel", 500, 800);
	}

	@Override
	public void recreateTabs() {
		guiWindow.activeInterface = this;
		createAegisTab();
	}

	/**
	 * Updates the aegis system stats and status. Use before drawing the UI or when any changes are made to the system.
	 *
	 * @param systemStats The system stats
	 * @param status The system status
	 */
	public void updateInfo(VirtualAegisSystem.AegisSystemStats systemStats, VirtualAegisSystem.AegisSystemStatus status) {
		this.systemStats = systemStats;
		this.status = status;
	}

	private GUIContentPane createAegisTab() {
		GUIContentPane contentPane;
		if(systemStats == null) contentPane = guiWindow.addTab("[ERROR] UNABLE TO BUILD WINDOW: SYSTEM STATS NOT AVAILABLE");
		else {
			if(!guiWindow.getTabs().isEmpty()) guiWindow.clearTabs();
			contentPane = guiWindow.addTab("AEGIS SYSTEM");
			windowWidth = guiWindow.getInnerWidthTab();
			windowHeight = guiWindow.getInnerHeigth();

			int leftWidth = (int) (windowWidth / 1.5f);
			int rightWidth = windowWidth - leftWidth;
			int bottomHeight = (int) (windowHeight / 2.0f);
			int topHeight = windowHeight - bottomHeight;
			int labelHeight = FontLibrary.FontSize.BIG.getRowHeight() + 2;

			contentPane.addDivider(rightWidth); //Divider between subsystems and schedule sections.
			contentPane.setTextBoxHeightLast(windowHeight);

			{ //Subsystems
				contentPane.setTextBoxHeight(0, 0, windowHeight);

				GUITextOverlay labelOverlay = new GUITextOverlay(leftWidth, labelHeight, getState());
				labelOverlay.setFont(FontLibrary.FontSize.BIG.getFont());
				labelOverlay.setColor(Color.cyan);
				labelOverlay.setTextSimple("SUBSYSTEMS");
				labelOverlay.onInit();
				contentPane.getContent(0, 0).attach(labelOverlay);

				String text = systemStats.getSubsystemInfoText();
				int textPanelHeight = FontLibrary.getFont(FontLibrary.FontSize.SMALL).getHeight(text);
				GUIScrollableTextPanel subsystemsTextPanel = new GUIScrollableTextPanel(leftWidth, textPanelHeight, contentPane.getContent(0, 0), getState());
				subsystemsTextPanel.setLeftRightClipOnly = false;
				GUITextOverlay subsystemsTextOverlay = new GUITextOverlay(leftWidth, textPanelHeight, getState());
				subsystemsTextOverlay.setFont(FontLibrary.FontSize.SMALL.getFont());
				subsystemsTextOverlay.setTextSimple(text);
				subsystemsTextOverlay.onInit();
				subsystemsTextPanel.setContent(subsystemsTextOverlay);
				subsystemsTextPanel.onInit();
				contentPane.getContent(0, 0).attach(subsystemsTextPanel);
				subsystemsTextPanel.getPos().y += labelHeight; //Move text panel down a bit to leave room for the label.
				subsystemsTextPanel.setHeight(subsystemsTextPanel.getHeight() - labelHeight); //Re-adjust height.
			}

			{ //Schedule
				GUITextOverlay labelOverlay = new GUITextOverlay(rightWidth, labelHeight, getState());
				labelOverlay.setFont(FontLibrary.FontSize.BIG.getFont());
				labelOverlay.setColor(Color.cyan);
				labelOverlay.setTextSimple("SCHEDULE");
				labelOverlay.onInit();
				contentPane.getContent(1, 0).attach(labelOverlay);
				AegisRecalibrationElement recalibrationElement = new AegisRecalibrationElement(getState(), (int) (contentPane.getContent(1, 0).getWidth() - 12), 30, systemStats, status);
				recalibrationElement.onInit();
				contentPane.getContent(1, 0).attach(recalibrationElement);
				recalibrationElement.getPos().y += labelHeight ; //Move this down a bit to leave room for the label.
				recalibrationElement.setHeight(recalibrationElement.getHeight() - labelHeight); //Re-adjust height.

				contentPane.addNewTextBox(1, 30); //Add another text box to the content pane to hold the recalibration button.
				GUITextButton recalibrateButton = getRecalibrateButton(contentPane.getContent(1, 1));
				contentPane.getContent(1, 1).attach(recalibrateButton);
				contentPane.setTextBoxHeight(1, 1, (int) recalibrateButton.getHeight());
			}

			{ //Status
				contentPane.addNewTextBox(1, bottomHeight); //Divider between schedule and status sections.
				AegisSystemStatusScrollableList statusScrollableList = new AegisSystemStatusScrollableList(getState(), contentPane.getContent(1, 2), systemStats, status);
				statusScrollableList.onInit();
				contentPane.getContent(1, 2).attach(statusScrollableList);

				contentPane.addNewTextBox(1, 30); //Add another text box to hold the status text.
				String statusText = "TODO"; //Todo: Add status text.
				int textHeight = FontLibrary.getFont(FontLibrary.FontSize.MEDIUM).getHeight(statusText);
				GUITextOverlay statusTextOverlay = new GUITextOverlay(rightWidth, textHeight, getState());
				statusTextOverlay.setFont(FontLibrary.FontSize.MEDIUM.getFont());
				//statusTextOverlay.setTextSimple(systemStats.getStatusText()); Todo: Add a method for this.
				statusTextOverlay.setColor(Color.red); //Todo: Change color based on status.
				statusTextOverlay.onInit();
				contentPane.getContent(1, 3).attach(statusTextOverlay);
				contentPane.addNewTextBox(1, 50); //Add another text box to hold the activation button.
				GUITextButton activationButton = getActivationButton(contentPane.getContent(1, 4));
				contentPane.getContent(1, 4).attach(activationButton);
				contentPane.setTextBoxHeight(1, 4, (int) activationButton.getHeight());
			}
		}
		return contentPane;
	}

	private GUITextButton getRecalibrateButton(GUIAncor contentPane) {
		if(systemStats.getTimeChangeCooldownRemaining() > 0) {
			return new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "CHANGE RECALIBRATION TIME\n(COOLING DOWN: " + StringTools.formatTimeFromMS(systemStats.getTimeChangeCooldownRemaining()) + ": NEW TIME WILL BE QUEUED)", new GUICallback() {
				@Override
				public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
					if(mouseEvent.pressedLeftMouse()) {
						//Todo: Queue recalibration time change request.
					}
				}

				@Override
				public boolean isOccluded() {
					return false;
				}
			});
		} else {
			return new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "CHANGE RECALIBRATION TIME", new GUICallback() {
				@Override
				public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
					if(mouseEvent.pressedLeftMouse()) {
						//Todo: Change recalibration time.
					}
				}

				@Override
				public boolean isOccluded() {
					return false;
				}
			});
		}
	}

	private GUITextButton getActivationButton(GUIAncor contentPane) {
		GUITextButton button = null;
		switch(status) {
			case ACTIVE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "DEACTIVATE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						if(mouseEvent.pressedLeftMouse()) {
							//Todo: Deactivate the system here
						}
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.OK);
				break;
			case ACTIVE_HACKED:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "DEACTIVATE\n(NOT ALLOWED: SYSTEM IS DISRUPTED)", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Do nothing
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.HOSTILE);
				break;
			case VULNERABLE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "VULNERABLE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.CANCEL);
				break;
			case ONLINING: //Not a word really, but it's a good enough description of the state.
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "POWERING UP", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.TUTORIAL);
				break;
			case NO_CHARGE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "INSUFFICIENT CHARGE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Todo: IDK
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.NEUTRAL);
				break;
			case DISABLED:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "ACTIVATE", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						if(mouseEvent.pressedLeftMouse()) {
							//Todo: Activate the system here
						}
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return false;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.FRIENDLY);
				break;
			case ERROR_STALE:
				button = new GUITextButton(getState(), (int) contentPane.getWidth(), 50, "ERROR", new GUICallback() {
					@Override
					public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
						//Do nothing
					}

					@Override
					public boolean isOccluded() { //Is the button not pressable?
						return true;
					}
				});
				button.setColorPalette(GUITextButton.ColorPalette.HOSTILE);
				break;
		}
		return button;
	}
}
