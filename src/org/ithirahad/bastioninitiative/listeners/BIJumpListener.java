package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import api.listener.events.entity.ShipJumpEngageEvent;
import api.utils.sound.AudioUtils;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLInterdictor;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.schine.common.language.Lng;

import javax.vecmath.Vector3f;
import java.util.HashSet;
import java.util.Iterator;

import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.warpSpaceIsPresentAndActive;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BIJumpListener extends Listener<ShipJumpEngageEvent> {
    private final Vector3i tmpi = new Vector3i();
    private final Vector3f tmpf = new Vector3f();
    @Override
    public void onEvent(ShipJumpEngageEvent e) {
        if(!warpSpaceIsPresentAndActive && e.isServer()) {
            float d = Vector3i.getDisatance(e.getNewSector(),e.getOriginalSectorPos());
            HashSet<Vector3i> systemPositions = new HashSet<>();
            for(int i = 0; i < d; i++){
                tmpf.set(MiscUtils.lerp(e.getOriginalSectorPos().toVector3f(), e.getNewSector().toVector3f(),(i/d)));
                tmpi.set(Math.round(tmpf.x),Math.round(tmpf.y),Math.round(tmpf.z));
                systemPositions.add(StellarSystem.getPosFromSector(tmpi, new Vector3i()));
                //This is really wasteful, but it's only done once per jump... so meh.
                //Ideally it'd be run in a separate thread during the jump charge-up time, but alas... not my game..
            }
            HashSet<VirtualFTLInterdictor> possibleInterdictors = new HashSet<>();
            for(Vector3i pos : systemPositions){
                HashSet<VirtualAegisSystem> agsystems = bsiContainer.getAegisSystemsInSystem(pos);

                if(agsystems != null) for(VirtualAegisSystem agsystem : agsystems) {
                    if(!BIUtils.isSelfOrAlly(e.getController().getFactionId(),agsystem.getFactionID())) {
                        VirtualFTLInterdictor candidate = (VirtualFTLInterdictor) agsystem.getSubsystem("Aegis Anti FTL");
                        if(candidate != null && candidate.isOperating(true)){
                            if(candidate.isInRange(e.getOriginalSectorPos()) || candidate.isInRange(e.getNewSector()) || candidate.crossesRange(e.getOriginalSectorPos(),e.getNewSector()))
                                possibleInterdictors.add(candidate);
                        }  //may as well skip over allied systems now rather than later to save on aegis system updates (and potential cascades)
                    }
                }
            }

            if(!possibleInterdictors.isEmpty()) {
                Iterator<VirtualFTLInterdictor> interdictors = possibleInterdictors.iterator();

                VirtualFTLInterdictor biggestDic = interdictors.next();
                VirtualFTLInterdictor thisDic;
                while (interdictors.hasNext()) {
                    thisDic = interdictors.next();
                    if (thisDic.getRange() > biggestDic.getRange()) biggestDic = thisDic;
                }
                e.getNewSector().set(biggestDic.getParent().getSectorLocation());
                for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>)e.getController()).getAttachedPlayers()) {
                    //TODO: time-delayed action, sound effect, particle effects, etc
                    playerAboard.sendServerMessage(Lng.astr("WARNING: Jump field captured by warp interceptor! Ship is being pulled in!"), MESSAGE_TYPE_INFO);
                }
                AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, ((ManagedUsableSegmentController<?>)e.getController()).getAttachedPlayers());
            }
        }
    }
}
