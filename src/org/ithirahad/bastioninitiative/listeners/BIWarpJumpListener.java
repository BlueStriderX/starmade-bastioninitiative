package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import com.bulletphysics.dynamics.RigidBody;
import me.iron.WarpSpace.Mod.WarpJumpEvent;
import org.ithirahad.bastioninitiative.persistence.VirtualFTLInterdictor;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.common.language.Lng;

import java.util.HashMap;

import static me.iron.WarpSpace.Mod.WarpJumpEvent.WarpJumpType.ENTRY;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BIWarpJumpListener extends Listener<WarpJumpEvent> {
    private static final HashMap<String,VirtualFTLInterdictor> dropCache = new HashMap<>();

    public static void enqueueDropLocation(String uniqueIdentifier, VirtualFTLInterdictor targetLoc) {
        dropCache.put(uniqueIdentifier,targetLoc);
    }

    public static boolean shipQueued(String uniqueIdentifier) {
        return dropCache.containsKey(uniqueIdentifier);
    }

    @Override
    public void onEvent(WarpJumpEvent e) {
        if(e.isServer()){
            if(e.getType() != ENTRY) {
                String uid = e.getShip().getUniqueIdentifier();
                if (dropCache.containsKey(uid)) {
                    VirtualFTLInterdictor vfd = dropCache.get(uid);
                    e.getEnd().set(vfd.getParent().getSectorLocation());
                    RigidBody body = e.getShip().getPhysicsObject();
                    float originalLin = body.getLinearDamping();
                    float originalAng = body.getAngularDamping();
                    body.setDamping(1.0f, 1.0f);
                    body.applyDamping(1);
                    body.setDamping(originalLin, originalAng);
                    body.activate(true); //stop the boat.

                    for (PlayerState playerAboard : ((ManagedUsableSegmentController<?>) e.getShip()).getAttachedPlayers()) {
                        playerAboard.sendServerMessage(Lng.astr("Your vessel has been pulled to the FTL Interceptor at " + vfd.getParent().getSectorLocation() + "."), MESSAGE_TYPE_INFO);
                    }
                    dropCache.remove(uid);
                }
            }
        }
    }
}
