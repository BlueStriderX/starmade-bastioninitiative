package org.ithirahad.bastioninitiative.listeners;

import api.listener.Listener;
import api.listener.events.weapon.ExplosionEvent;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.DamageDealerType;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.controller.elements.ShieldContainerInterface;
import org.schema.game.common.data.creature.AICharacter;
import org.schema.game.common.data.player.PlayerCharacter;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.network.objects.Sendable;

import java.util.Iterator;

import static java.lang.Math.max;
import static org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.BastionField.isSegmentControllerInvulnerable;
import static org.schema.game.common.controller.damage.effects.InterEffectHandler.InterEffectType.*;

public class BIExplosionListener extends Listener<ExplosionEvent> {
        //TODO: maybe we can just strip relevant entities out of the hit buffer and do away with all of this shit

        //TODO: If necessary, create new callbacks with appropriate functionality.
        // (Conveniently, all they know at this stage is where the explosion happens in world space,
        // and they don't know anything about what they hit, so it's not going to be that difficult to
        // reconstruct them midstream if we have to. Kinda sucks but oh well.)

        @Override
        public void onEvent(ExplosionEvent e) {
            ObjectArrayList<Sendable> hitBuffer = (ObjectArrayList<Sendable>) MiscUtils.readPrivateField("hitBuffer",e.getExplosionRunnable());
            Object2DoubleOpenHashMap<ShieldContainerInterface> shieldDamages = new Object2DoubleOpenHashMap<>(); //damage to do to shields
            Iterator<Sendable> shIterator = hitBuffer.iterator(); //Iterator through all "Sendables" that the missile has ostensibly hit.
            boolean absorbed = false;
            // (of course, this clearly stands for "sendable-hit iterator", what else do you think I meant?)
            while(shIterator.hasNext()){
                Sendable sendable = shIterator.next();
                SegmentController sc = null;
                if (sendable instanceof SegmentController) {
                    sc = (SegmentController)sendable;
                } else if(sendable instanceof PlayerCharacter) {
                    SimpleTransformableSendableObject<?> entityAttachedTo = ((PlayerCharacter) sendable).getGravity().source;
                    if (entityAttachedTo instanceof SegmentController) sc = (SegmentController) entityAttachedTo; //instanceof covers null check too
                } else if(sendable instanceof AICharacter) {
                    SimpleTransformableSendableObject<?> entityAttachedTo = ((AICharacter) sendable).getGravity().source;
                    if (entityAttachedTo instanceof SegmentController) sc = (SegmentController) entityAttachedTo;
                }

                if(sc != null){
                    boolean isActiveTurret = (sc.railController.isTurretDocked() && sc.isAIControlled());
                    if(!isActiveTurret) {
                        SegmentController root;
                        root = sc.railController.getRoot();
                        if (isSegmentControllerInvulnerable(root)) {
                            boolean attackOtherwiseAllowed = sc.checkAttack(e.getExplosion().from, true, e.getExplosion().damageType != DamageDealerType.EXPLOSIVE);
                            //Do usual housekeeping stuff for a missile hit, e.g. notify faction as if this were a weapon hit
                            if (sendable instanceof SegmentController && !e.getExplosion().ignoreShields && root instanceof ManagedUsableSegmentController<?> && attackOtherwiseAllowed) {
                                //sendable instanceof check because we don't want to double-add if it's a playercharacter who is protected - that'd be a pointless waste
                                shieldDamages.add((ShieldContainerInterface) ((ManagedUsableSegmentController<?>) root).getManagerContainer(), e.getExplosion().damageInitial);
                            } //else it's a bomb or a mine or something weird; ignore it.
                            absorbed = true;
                            shIterator.remove(); //Prevent explosion physics from looking at the entity
                        }
                    }
                }
            }
            if(absorbed){
                int size = shieldDamages.size();
                for(ShieldContainerInterface shieldSystem : shieldDamages.keySet()){
                    double damage = shieldDamages.get(shieldSystem)/size;
                    try {
                        InterEffectSet shields = shieldSystem.getSegmentController().getEffectContainer().get(HitReceiverType.SHIELD);
                        InterEffectSet attack = e.getExplosion().attackEffectSet;
                        double modifier = 0;
                        modifier += max(0,attack.getStrength(EM) - shields.getStrength(EM));
                        modifier += max(0,attack.getStrength(HEAT) - shields.getStrength(HEAT));
                        modifier += max(0,attack.getStrength(KIN) - shields.getStrength(KIN));
                        //TODO: modifier /= 3?
                        shieldSystem.getShieldAddOn().getShieldLocalAddOn().hitAllShields(damage * modifier);
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                    //TODO: force update all local shields if need be...
                }
            }
    }
}
