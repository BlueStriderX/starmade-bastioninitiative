package org.ithirahad.bastioninitiative.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import java.io.IOException;

import static org.ithirahad.bastioninitiative.util.BIUtils.factionAccessMayBeAllowed;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_WARNING;

/**
Packet sent by a client to the server when they desire to make changes to an Aegis System.
 <br\><b>NOTE: As an invulnerability system is especially prone to so-called "PvP" cheat and exploit shenanigans,
 serverside validation is <i>imperative</i> here, even down to (TODO) the player being within activation range of the block.</b>
 */
//for now, let's just be sure that the faction is right and they're actually in the sector. That's a start.
public class PlaceholderAegisUIInputPacket extends Packet {
    boolean requestedState = false; //true = on; false = off
    boolean simpleToggleRequest;
    String entityUID = "";
    long newCycleTime = 0;
    long blockAbsIndex = 0;

    //not doing anything with this for now, but eventually we'll confirm the block is an Aegis Core
    //and calculate the relative positions of the block and the player trying to access it on serverside
    //to try and weed out a lot of potential shenaniganeering.

    public PlaceholderAegisUIInputPacket(boolean requestIsForActivation, String entity, long absoluteIndex){
        requestedState = requestIsForActivation;
        this.entityUID = entity;
        blockAbsIndex = absoluteIndex;
        simpleToggleRequest = true;
        newCycleTime = now() - getLastMidnight();
    }

    public PlaceholderAegisUIInputPacket(){}

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        entityUID = b.readString();
        blockAbsIndex = b.readLong();
        requestedState = b.readBoolean();
        simpleToggleRequest = b.readBoolean();
        newCycleTime = b.readLong();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeString(entityUID);
        b.writeLong(blockAbsIndex);
        b.writeBoolean(requestedState);
        b.writeBoolean(simpleToggleRequest);
        b.writeLong(newCycleTime);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        SegmentController sc = GameServerState.instance.getSegmentControllersByName().get(entityUID);
        if(!(sc == null) && validOnServer(sc,playerState)) {
            short coreID = elementEntries.get("Aegis Core").id;
            if(sc.getSegmentBuffer().getPointUnsave(blockAbsIndex).getType() == coreID) //TODO: && worldPosition(sc,blockAbsIndex).sub(getPlayerWorldPosition(playerState)) <= activationRadiusMax)
            { //we can be reasonably sure that there is no direct tampering going on here
                AegisCore aegisCore = (AegisCore) ((ManagedUsableSegmentController<?>) sc).getManagerContainer().getModMCModule(coreID);
                //validated
                aegisCore.processActivationInput(requestedState);
                if (simpleToggleRequest) {
                    aegisCore.setNewRefresh(currentTimeOfDay());
                }
                else
                {
                    aegisCore.setNewRefresh(newCycleTime);
                }
            }
            else playerState.sendServerMessage(Lng.astr(sc.getRealName() + " does not have an Aegis Core here! Are you tampering with your local block data? You cheeky bugger, you..."), MESSAGE_TYPE_WARNING);
        }
    }

    private boolean validOnServer(SegmentController sc, PlayerState playerState){
        return sc.getSegmentBuffer().getPointUnsave(blockAbsIndex).getType() == elementEntries.get("Aegis Core").id &&
                factionAccessMayBeAllowed(playerState.getFactionId(),sc.getFactionId())  &&
                sc.getSector(new Vector3i()).equals(playerState.getCurrentSector());
    }

    @Override
    public void processPacketOnClient() {/*How did we even get here?*/}
}
