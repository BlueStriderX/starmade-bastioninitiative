package org.ithirahad.bastioninitiative.persistence;

import api.common.GameCommon;
import api.common.GameServer;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.BIConfiguration;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.database.DatabaseEntry;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.server.ServerMessage;

import java.io.Serializable;
import java.util.*;

import static java.lang.Math.max;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.*;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_DAYS;
import static org.ithirahad.bastioninitiative.util.Constants.STATUS_UPDATE_MAX_ITERATIONS;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.*;

/**
 * Persistent class which tracks the overall operational status, disruption status, and Aegis Charge consumption of an aegis system.
 */
public class VirtualAegisSystem implements Serializable {
    public final String uid;
    private final Vector3i location;
    private int faction;

    private int chargeConsPerDay;
    private int chargeConsToOnline;

    public enum AegisSystemStatus {
        ACTIVE("Active"), //System online
        ACTIVE_HACKED("Active <Disrupted>"), //System was disrupted during current cycle; will go vulnerable
        VULNERABLE("Rebooting"), //Cycle passed after disruption
        ONLINING("Awaiting Activation Charge"), //System activated but awaiting sufficiant charge to go online
        NO_CHARGE("Insufficient Charge"), //System was active, but did not have charge
        DISABLED("Inactive"),
        ERROR_STALE("[MOD ERROR] Ghost System; Should Not Be Accessible");

        private final String niceName;

        AegisSystemStatus(String s){
            niceName = s;
        }

        public String getNiceName() {
            return niceName;
        }
    }

    public static class AegisSystemStats {

        private String subsystemInfoText;
        private long recalibrationStart;
        private long recalibrationDuration;
        private boolean isDisrupted;
        private long timeChangeCooldownRemaining;
        private long queuedTimeChange;
        private int aegisSystemActivationCost;
        private int aegisSystemDailyCost;
        private double aegisChargeStored;
        private int aegisChargeCapacity;
        private long fuelledUntil;

        public AegisSystemStats(String subsystemInfoText, long recalibrationStart, long recalibrationDuration, boolean isDisrupted, long timeChangeCooldownRemaining, long queuedTimeChange, int aegisSystemActivationCost, int aegisSystemDailyCost, double aegisChargeStored, int aegisChargeCapacity, long fuelledUntil) {
            this.subsystemInfoText = subsystemInfoText;
            this.recalibrationStart = recalibrationStart;
            this.recalibrationDuration = recalibrationDuration;
            this.isDisrupted = isDisrupted;
            this.timeChangeCooldownRemaining = timeChangeCooldownRemaining;
            this.queuedTimeChange = queuedTimeChange;
            this.aegisSystemActivationCost = aegisSystemActivationCost;
            this.aegisSystemDailyCost = aegisSystemDailyCost;
            this.aegisChargeStored = aegisChargeStored;
            this.aegisChargeCapacity = aegisChargeCapacity;
            this.fuelledUntil = fuelledUntil;
        }

        public String getSubsystemInfoText() {
            return subsystemInfoText;
        }

        public void setSubsystemInfoText(String subsystemInfoText) {
            this.subsystemInfoText = subsystemInfoText;
        }

        public long getRecalibrationStart() {
            return recalibrationStart;
        }

        public void setRecalibrationStart(long recalibrationStart) {
            this.recalibrationStart = recalibrationStart;
        }

        public long getRecalibrationDuration() {
            return recalibrationDuration;
        }

        public void setRecalibrationDuration(long recalibrationDuration) {
            this.recalibrationDuration = recalibrationDuration;
        }

        public boolean isDisrupted() {
            return isDisrupted;
        }

        public void setDisrupted(boolean disrupted) {
            isDisrupted = disrupted;
        }

        public long getTimeChangeCooldownRemaining() {
            return timeChangeCooldownRemaining;
        }

        public void setTimeChangeCooldownRemaining(long timeChangeCooldownRemaining) {
            this.timeChangeCooldownRemaining = timeChangeCooldownRemaining;
        }

        public long getQueuedTimeChange() {
            return queuedTimeChange;
        }

        public void setQueuedTimeChange(long queuedTimeChange) {
            this.queuedTimeChange = queuedTimeChange;
        }

        public int getAegisSystemActivationCost() {
            return aegisSystemActivationCost;
        }

        public void setAegisSystemActivationCost(int aegisSystemActivationCost) {
            this.aegisSystemActivationCost = aegisSystemActivationCost;
        }

        public int getAegisSystemDailyCost() {
            return aegisSystemDailyCost;
        }

        public void setAegisSystemDailyCost(int aegisSystemDailyCost) {
            this.aegisSystemDailyCost = aegisSystemDailyCost;
        }

        public double getAegisChargeStored() {
            return aegisChargeStored;
        }

        public void setAegisChargeStored(double aegisChargeStored) {
            this.aegisChargeStored = aegisChargeStored;
        }

        public int getAegisChargeCapacity() {
            return aegisChargeCapacity;
        }

        public void setAegisChargeCapacity(int aegisChargeCapacity) {
            this.aegisChargeCapacity = aegisChargeCapacity;
        }

        public long getFuelledUntil() {
            return fuelledUntil;
        }

        public void setFuelledUntil(long fuelledUntil) {
            this.fuelledUntil = fuelledUntil;
        }
    }

    private AegisSystemStatus status = DISABLED;
    private boolean disrupted = false;
    private double aegisCharge = 0; //TODO: backup list for this so random issues don't cause people to lose ungodly amounts of fuel

    transient private long lastUpdate;
    private long lastDisrupt = 0;
    public int disruptionDurationMS = DISRUPTION_BASE_DURATION_MS; //how long (in MS) to wait before reactivating if disrupted
    private long latestUpTime = 0; //total time since last activation from inactive/unfuelled state
    private long recycleTime = 79200000; //time in GMT (in MS) when the system recycles.
    // Defaults to 10:00 PM GMT/6:00 PM EST, i.e. compromise transatlantic time

    //TODO: Longs as time is nice, but we have to be careful about solar years and such for long-term.
    //easiest way is to start with a Time-based date 00:00:00 and do our MS from there.

    //TODO: replace all of these weird time comparisons with a countdown timer object of some sort, where applicable

    public long newRecycleTime; //new cycle time
    private boolean queuedForChange; //supposed to be authoritative; validate BEFORE setting this
    public int changeCycleCounter = 0; //after this many cycles, allow a cycle time change (currentRecycleTime = new Time(nextRecycleTime))
    public long lastCycleTimeChange;
    public long prevRecycleTime = 0;

    transient private HashMap<String, VirtualAegisSubsystem> subsystems = new HashMap<>(); //full of subclass objects; GSON does not like this
    private Collection<VirtualAegisSubsystemCrate> persistencePack = new ArrayList<>();

    public VirtualAegisSystem(SegmentController segmentController) {
        uid = DatabaseEntry.removePrefix(segmentController.getUniqueIdentifier());
        location = segmentController.getSector(new Vector3i());
        faction = segmentController.getFactionId();
        lastUpdate = now();
        if(GameCommon.isClientConnectedToServer()) throw new RuntimeException(){
            @Override
            public String getMessage() {
                return "[MOD][BastionInitiative] Attempted to instantiate server-based virtual object on dedicated client!" + super.getMessage();
            }
        };
    }

    public void beforeWriteToPersistence(){
        persistencePack.clear();
        for(String key : subsystems.keySet()){
            VirtualAegisSubsystem ssys = subsystems.get(key);
            persistencePack.add(new VirtualAegisSubsystemCrate(key,ssys));
        }
        //TODO: Since the persistence packs are unreadable gobbledygook, we must eventually write a log listing all the subsystems' relevant properties for debugging.
    }

    public void onLoadFromPersistence(){
        rollLastUpdateToNow(); //don't consume fuel for time offline
        subsystems = new HashMap<>();
        for(VirtualAegisSubsystemCrate pak : persistencePack){
            try {
                pak.retrieveTo(subsystems);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        for(VirtualAegisSubsystem s : subsystems.values()){
            s.setParent(this); //double-sided links probably wouldn't survive serialization, so we just set it transient and reconnect it here when the subsystems are unrolled
            s.onLoadFromPersistence();
        }
    }

    public VirtualAegisSubsystem getSubsystem(String name){
        return (VirtualAegisSubsystem) subsystems.get(name);
    }

    public boolean hasSubsystem(String name) {
        return subsystems.containsKey(name);
    }

    public void putSubsystem(String elementEntriesName, VirtualAegisSubsystem subsystem){
        subsystems.put(elementEntriesName,subsystem);
        subsystem.setParent(this);
    }

    public void removeSubsystem(String elementEntriesName) {
        subsystems.remove(elementEntriesName);
    }

    public void DEBUG_clearDisruptInfo() {
        disrupted = false;
        lastDisrupt = 0;
    }

    public void changeStatus(AegisSystemStatus v) {
        status = v;
        update();
    }

    public AegisSystemStatus getStatus(){
        return status;
    }

    public boolean isDisrupted() {
        return status == ACTIVE_HACKED || status == VULNERABLE;
    }

    public void giveCharge(int val){
        if(val < 0) {
            System.err.println("[MOD][BastionInitiative][WARNING] Attempted to give negative amounts of charge to an Aegis Nexus. What happened here?");
        }
        aegisCharge += val;
    }

    public void update() {
        if(queuedForChange && isTimeChangeAllowed()){
            setRecycleTime(newRecycleTime);
            queuedForChange = false;
            newRecycleTime = -1;
            lastCycleTimeChange = now();
        }

        //---------------------------------------------------------------------------------
        AegisSystemStatus originalStatus = status;
        double originalFuel = aegisCharge;
        AegisSystemStatus previousStatus;
        int loops = 0;
        //int chargePerCycle = calcAegisChargePerCycle(); //TODO do this in MMCM
        do{
            long now = now();
            long zeroToday = getLastMidnight();
            long clockTimeMs = now - zeroToday; //time since 0000 GMT today
            // Java rounds down, so the remainder of a day partially elapsed before the previous update shouldn't prompt extra fuel usage.

            long msSincePrevUpdate = now - lastUpdate; //time since last update()
            long postCycleTime = max(0L,clockTimeMs - recycleTime); //amount of time that has passed since the most recent recycle time
            int cyclesCompletedSinceUpdate = (int) max(0,msSincePrevUpdate-postCycleTime) / MS_TO_DAYS; //Number of days from last update to last cycle turnover.
            if(postCycleTime < msSincePrevUpdate) cyclesCompletedSinceUpdate++;

            previousStatus = status;
            switch(status) {
                //TODO: Switch this to something that is not an enum; enum waterfall logic is jank considering how complex the graph of this is
                case ONLINING:
                    if(aegisCharge >= chargeConsToOnline){
                        //if we have enough to online, then we (at some point) went online. Next state can handle real-time fuel usage, whatever it is.
                        aegisCharge -= chargeConsToOnline;
                        if(disrupted) status = ACTIVE_HACKED;
                        else status = ACTIVE;
                        break;
                        //this has the useful side effect of cancelling out any "completed" cycles while onlining - it's not actually active, so it shouldn't consume any aegis yet other than the activation cost
                    }
                    else break; //just sit here waiting for that sweet, sweet Aegis juice
                case ACTIVE_HACKED:
                    if(cyclesCompletedSinceUpdate == 0){
                        //we're still in the cycle when the thing was hacked. Consume fuel, otherwise no action.
                        // (I hope that system time is accurate enough to do it like this, else we have to use lastDisrupt instead)
                        if(!tryConsumeChargeToRun(msSincePrevUpdate)) status = NO_CHARGE;
                        else latestUpTime += msSincePrevUpdate; //still going
                        break;
                    }
                    else if (cyclesCompletedSinceUpdate == 1 && postCycleTime < disruptionDurationMS) {
                        //we're in the vulnerability window; set to vulnerable.
                        tryConsumeChargeToRun(msSincePrevUpdate); //don't care about the return value here; if it ran out at some point then VULNERABLE is still correct
                        status = VULNERABLE;
                        break;
                    }
                    else status = ACTIVE; //...and so we fall through
                case ACTIVE:
                    if (disrupted){
                        status = ACTIVE_HACKED; //let that state take care of fuel consumption next iteration
                    } else {
                        if (tryConsumeChargeToRun(msSincePrevUpdate)) {
                            latestUpTime += msSincePrevUpdate; //still going
                        } else status = NO_CHARGE;
                        disrupted = false;
                    }
                    break;
                case VULNERABLE:
                    long timeSinceDis = now() - lastDisrupt;
                    if(postCycleTime > disruptionDurationMS || timeSinceDis > MS_TO_DAYS) {
                        status = ACTIVE;
                        latestUpTime = (postCycleTime - disruptionDurationMS) + (MS_TO_DAYS * (timeSinceDis/MS_TO_DAYS));
                        //unless a later state behaviour says otherwise, we've been active since the window cleared.
                    }
                    break;
                case NO_CHARGE:
                    latestUpTime = 0;
                    if(tryConsumeChargeToRun(msSincePrevUpdate)){
                        if(disrupted) status = ACTIVE_HACKED;
                        //that state can clear itself next iteration if the most recent disruption was too long ago, or go vuln if needed,
                        // so it's the fastest and safest bet.
                        // We could technically go there no matter what, but to save ourselves an iteration:
                        else status = ACTIVE;
                    }
                    break;
                case DISABLED:
                    latestUpTime = 0;
                    break;
                case ERROR_STALE:
                    latestUpTime = 0;
                    return;
            }
            lastUpdate = now();
            loops++;
        } while(status != previousStatus && loops < STATUS_UPDATE_MAX_ITERATIONS); //continue iterating while status is changing

        if(loops == STATUS_UPDATE_MAX_ITERATIONS){
            for (RegisteredClientOnServer client : GameServer.getServerState().getClients().values()) {
                try {
                    PlayerState player =  GameServer.getServerState().getPlayerFromName(client.getPlayerName());
                    if(player.isAdmin()) player.sendServerMessage(Lng.astr("[SERVER][BastionInitiative] \r\n Long update loop terminated for entity " + uid + " in state: " + status.name()), ServerMessage.MESSAGE_TYPE_WARNING);
                } catch (PlayerNotFountException ignore) {}
            }
        }

        if(originalStatus != NO_CHARGE && status == NO_CHARGE){
            sendNoFuelNotification();
        }else {
            if(getMaxChargeCapacity() > 0) {
                int lowFuelLevel = (int) (getChargeConsPerDay() * LOW_FUEL_ALERT_FACTOR);
                if (originalFuel >= lowFuelLevel && aegisCharge < lowFuelLevel) {
                    sendLowFuelNotification();
                }
            }
        }

        //TODO: Update consumption rate in receiver registry.
    }

    private void sendNoFuelNotification() {

    }

    private void sendLowFuelNotification() {

    }

    public int getMaxChargeCapacity() {
        return AEGIS_CORE_CELL_CAPACITY; //for now. this might eventually be a variable
    }

    /**
     * Consume an amount of fuel corresponding to the amount of time that passed since last update.
     * @param ms Amount of time worth of fuel to try consuming
     * @return whether sufficient fuel was present
     */
    private boolean tryConsumeChargeToRun(long ms) {
        double amount = (((double)ms)/MS_TO_DAYS) * getChargeConsPerDay(); //per cycle means per day basically, so we need the fraction of a day
        //TODO: transmitter changes this. should be (amount > getAvailableCharge()) if transmitter is passive.
        // else if enough remote charge use that; else use remote then local.
        if(amount > aegisCharge){
            aegisCharge = 0;
            return false;
        }
        else{
            aegisCharge -= amount;
            return true;
        }
    }

    public void setOrQueueNewRecycleTime(long v) {
        if(lastCycleTimeChange < now() - AEGIS_RECYCLE_TIME_CHANGE_COOLDOWN_DAYS) {
            recycleTime = v;
            lastCycleTimeChange = now();
            queuedForChange = false;
            newRecycleTime = -1;
        }
        else
        {
            newRecycleTime = v;
            queuedForChange = true;
        }
    }

    public void tryDisrupt() {
        if(((now() - lastDisrupt)/MS_TO_DAYS) > DISRUPTION_SAFETY_DAYS) {
            disrupted = true;
            lastDisrupt = now();
            update();
        }
    }

    public void handleDestroyed() {
        if(status == VULNERABLE){
            //TODO: if system has hub, update hub's most recent energy backlash time to now.
            // (that way the hub can't just start linking to new systems again instantly)
        }

        bsiContainer.removeAegisSystem(this);
        changeStatus(ERROR_STALE); //if it persists, we can see that it's a bug

        for (Iterator<String> keys = subsystems.keySet().iterator(); keys.hasNext(); ) {
            String s = keys.next();
            subsystems.get(s).onDestroy();
            keys.remove();
        }
    }

    public int getChargeConsToPutOnline() {
        return chargeConsToOnline;
    }

    public void setChargeConsRequiredToOnLine(int chargeConsToOnline) {
        this.chargeConsToOnline = chargeConsToOnline;
    }

    public double getAegisCharge() {
        return aegisCharge;
    }

    public void setCharge(int i) {
        aegisCharge = i;
    }

    public Vector3i getSectorLocation() {
        return new Vector3i(location);
    }

    @Override
    public int hashCode() {
        return ("CONS" + uid).hashCode();
    }

    public int getChargeConsPerDay(){
        return chargeConsPerDay;
    }

    public void setChargeConsPerDay(int cost) {
        chargeConsPerDay = cost;
    }

    private void setRecycleTime(long t) {
        recycleTime = t;
    }

    public int getFactionID() {
        return faction;
    }

    public void setFaction(int faction) {
        this.faction = faction;
    }

    public Calendar getNextCycleTime() {
        Calendar result = new GregorianCalendar();
        result.setTimeInMillis(getLastMidnight());
        result.add(Calendar.MILLISECOND,(int)recycleTime);
        if(currentTimeOfDay() > recycleTime) result.roll(Calendar.DATE,1);
        if(queuedForChange){
            long timeUntilCycleTimeChanges = getTimeChangeAllowedThreshold() - now();
            long timeUntilNextCycle = result.getTimeInMillis();
            if(timeUntilCycleTimeChanges < timeUntilNextCycle){
                result.setTimeInMillis(getLastMidnight());
                result.add(Calendar.MILLISECOND,(int)newRecycleTime);
                if(currentTimeOfDay() > newRecycleTime) result.roll(Calendar.DATE,1);
            }
        } //TODO: optimize/debug?

        return result;
    }

    public long getTimeChangeCooldownRemaining() {
        return 0;
    }

    /**
     * @return the time after which the system will no longer have enough Aegis Charge to function. Among other things, this is useful for determining whether or not a system should act when unloaded, e.g. as a FTL inhibitor.
     */
    public long getFuelledUntil(){
        double chargeConsPerMs = ((double)chargeConsPerDay)/MS_TO_DAYS;
        return lastUpdate + (long) Math.floor(aegisCharge/chargeConsPerMs);
        //TODO: should this be cached?
    }

    /**
     * @return whether or not the system is providing Aegis Charge to its subsystems.
     * This will be true while the system is fuelled and not in a vulnerable or offline state.
     */
    public boolean isActive() {
        return (status == ACTIVE || status == ACTIVE_HACKED);
    }

    public void activate(){
        if(status == DISABLED || status == ERROR_STALE) {
            status = ONLINING;
            setOrQueueNewRecycleTime(now() - getLastMidnight());
            update();
        }
    }

    //allows time skips e.g. when game is offline and turns back on again
    public void rollLastUpdateToNow() {
        lastUpdate = now();
    }

    @NotImplemented
    public boolean hasBenefactorNexus(){
        return false;
    }

    public boolean isNexusOnlining(){return status == ONLINING;}

    public long getLatestUpTime() {
        return latestUpTime;
    }

    public long getLastUpdate(){
        return lastUpdate;
    }

    public long getLastCycleTimeChange() {
        return lastCycleTimeChange;
    }

    public long getLastDisrupt() {
        return lastDisrupt;
    }

    public long getRecycleTime() {
        return recycleTime;
    }

    public long getRecycleDuration() {
        return DISRUPTION_BASE_DURATION_MS;
    }


    public boolean isQueuedForChange() {
        return queuedForChange;
    }

    public long recycleTimeAfterChange() {
        return newRecycleTime;
    }

    public boolean isTimeChangeAllowed(){
        return !isDisrupted() && //tl;dr you can't get cucked out of a siege by a defender initiating a timely time change
                Calendar.getInstance().getTime().getTime() > getTimeChangeAllowedThreshold();
    }

    /**
    @return After what time the system will be allowed to change its recycle times
     */
    public long getTimeChangeAllowedThreshold(){
        return lastCycleTimeChange + ((long) BIConfiguration.AEGIS_RECYCLE_TIME_CHANGE_COOLDOWN_DAYS *MS_TO_DAYS);
    }

    public long getRemainingCyclesUntilDischarged(){
        update();
        if(aegisCharge > 0) return Math.round(Math.floor(aegisCharge / chargeConsPerDay));
        return 0;
    }

    public static class VirtualAegisSubsystemCrate implements Serializable {
        //lol
        //lmao
        //...Aiiiye, we actually are doing this. Aiya aelen syai se.
        transient private Class<? extends VirtualAegisSubsystem> cls;
        private byte[] object;
        private String label;
        private String className;

        VirtualAegisSubsystemCrate(String key, VirtualAegisSubsystem ssys){
            label = key;
            cls = ssys.getClass();
            object = SerializationUtils.serialize(cls.cast(ssys));
            className = cls.getName();
        }

        VirtualAegisSubsystemCrate(){};

        @SuppressWarnings("unchecked")
        void beforeUnpack() throws ClassNotFoundException, ClassCastException {
            cls = (Class<? extends VirtualAegisSubsystem>) Class.forName(className);
        }

        Class<? extends VirtualAegisSubsystem> getContainedClass(){
            return cls;
        }

        void retrieveTo(HashMap<String, VirtualAegisSubsystem> target) throws ClassNotFoundException {
            beforeUnpack();
            VirtualAegisSubsystem unpacked = cls.cast(SerializationUtils.deserialize(object));
            target.put(label,unpacked);
        }
    }
}
