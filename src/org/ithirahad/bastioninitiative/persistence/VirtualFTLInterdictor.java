package org.ithirahad.bastioninitiative.persistence;

import api.utils.sound.AudioUtils;
import me.iron.WarpSpace.Mod.WarpJumpManager;
import me.iron.WarpSpace.Mod.WarpManager;
import org.ithirahad.bastioninitiative.listeners.BIWarpJumpListener;
import org.ithirahad.bastioninitiative.util.BIUtils;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.common.language.Lng;

import java.util.HashSet;
import java.util.Iterator;

import static org.ithirahad.bastioninitiative.BastionInitiative.bsiContainer;
import static org.ithirahad.bastioninitiative.util.BIUtils.isSelfOrAlly;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class VirtualFTLInterdictor extends VirtualAegisSubsystem {
    private float range; //in sectors
    private transient boolean didFirstUpdate = false;
    //takes advantage of how deserialized transient values are set to their defaults - in this case, false.

    public VirtualFTLInterdictor(){}

    public boolean crossesRange(Vector3i start, Vector3i end){
        return rangeSquared() >= BIUtils.shortestDistanceToLineSegmentSquared(start,end,getParent().getSectorLocation());
    }

    public boolean isInRange(Vector3i sector){
        return rangeSquared() >= Vector3i.getDisatanceSquaredD(sector,getParent().getSectorLocation());
    }


    public boolean isInRangeWarp(Vector3i loc) {
        return rangeSquared() >= Vector3i.getDisatanceSquaredD(loc,WarpManager.getWarpSpacePos(getParent().getSectorLocation()));
    }

    private float rangeSquared() {
        return range*range;
    }

    public void setRange(float v){
        range = v;
    }

    @Override
    public String getBlockEntryName() {
        return null;
    }

    @Override
    public void onLoadFromPersistence() {

    }

    @Override
    public void onUpdate(boolean powered) {
        didFirstUpdate = true;
    }

    public float getRange() {
        return range;
    }

    public boolean neverUpdated() {
        return !didFirstUpdate;
    }

    public static void handleSegmentController(SegmentController sc) {
        if(!BIWarpJumpListener.shipQueued(sc.getUniqueIdentifier()) || !WarpJumpManager.dropQueue.contains(sc)) {
            ManagedUsableSegmentController<?> temmsc = (ManagedUsableSegmentController<?>) sc;
            Vector3i shipPos = new Vector3i();
            if (bsiContainer.hasWarpEntries(temmsc.getSector(shipPos))) { //also assigns to shipPos
                HashSet<VirtualFTLInterdictor> toCheck = new HashSet<>();
                for (VirtualAegisSystem sys : bsiContainer.getAegisSystemsInWarpSector(shipPos)) {
                    if (sys.isActive() && !isSelfOrAlly(sys.getFactionID(), temmsc.getFactionId())) {
                        VirtualFTLInterdictor candidate = (VirtualFTLInterdictor) sys.getSubsystem("Aegis Anti FTL");
                        if (candidate != null &&
                                candidate.isOperating(true) &&
                                candidate.isInRangeWarp(shipPos))
                            toCheck.add(candidate);
                    }
                }

                if (!toCheck.isEmpty()) {
                    Iterator<VirtualFTLInterdictor> dics = toCheck.iterator();

                    VirtualFTLInterdictor biggestDic = dics.next();
                    VirtualFTLInterdictor thisDic;
                    while (dics.hasNext()) {
                        thisDic = dics.next();
                        if (thisDic.getRange() > biggestDic.getRange()) biggestDic = thisDic;
                    }

                    String faction;
                    if (biggestDic.getFactionID() == 0) faction = "faction-neutral";
                    else
                        faction = GameServerState.instance.getFactionManager().getFactionName(biggestDic.getFactionID());
                    for (PlayerState playerAboard : temmsc.getAttachedPlayers()) {
                        playerAboard.sendServerMessage(Lng.astr("WARNING: Warp field destablilized by a " + faction + "  FTL interceptor system! Vessel is being returned to real space!"), MESSAGE_TYPE_INFO);
                    }
                    AudioUtils.serverPlaySound("0022_gameplay - low armor _electric_ warning chime", 1.0f, 1.0f, temmsc.getAttachedPlayers());

                    BIWarpJumpListener.enqueueDropLocation(temmsc.getUniqueIdentifier(), biggestDic);
                    WarpJumpManager.invokeDrop(3000, temmsc, true, false);
                }
            }
        } //else already about to get yeeted
    }
}
