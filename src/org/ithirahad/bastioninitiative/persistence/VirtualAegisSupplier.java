package org.ithirahad.bastioninitiative.persistence;

import org.apache.poi.util.NotImplemented;

@NotImplemented
public class VirtualAegisSupplier extends VirtualAegisSubsystem {
    private int output;
    private int efficiencyFactor;

    public VirtualAegisSupplier() {

    }

    public float getDailyOutputWithLosses() {
        return output * efficiencyFactor;
    }

    public float getDailyOutput() {
        return output;
    }

    @Override
    public String getBlockEntryName() {
        return "Aegis Transmitter";
    }

    @Override
    public void onLoadFromPersistence() {

    }

    @Override
    public void onUpdate(boolean powered) {

    }
}
