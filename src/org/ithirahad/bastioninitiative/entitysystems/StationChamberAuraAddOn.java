package org.ithirahad.bastioninitiative.entitysystems;

import api.mod.StarMod;
import api.utils.addon.SimpleAddOn;
import org.schema.game.common.controller.elements.ManagerContainer;

public class StationChamberAuraAddOn extends SimpleAddOn {
    private static final String name = "Station Remote Support System";
    public StationChamberAuraAddOn(ManagerContainer<?> managerContainer, short i, StarMod starMod, String s) {
        super(managerContainer, i, starMod, s);
    }

    @Override
    public float getChargeRateFull() {
        return 0;
    }

    @Override
    public double getPowerConsumedPerSecondResting() {
        return 0;
    }

    @Override
    public double getPowerConsumedPerSecondCharging() {
        return 0;
    }

    @Override
    public float getDuration() {
        return 0;
    }

    @Override
    public boolean onExecuteServer() {
        return false;
    }

    @Override
    public boolean onExecuteClient() {
        return false;
    }

    @Override
    public void onActive() {

    }

    @Override
    public void onInactive() {

    }

    @Override
    public String getName() {
        return name;
    }
}
