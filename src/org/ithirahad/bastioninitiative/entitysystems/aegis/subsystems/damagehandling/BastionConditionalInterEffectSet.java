package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.damagehandling;

import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.BastionField;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.effects.InterEffectHandler;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

import static org.ithirahad.bastioninitiative.BIStatusEffectManager.bastionFieldStatusEffect;

/**
 * A replication of BastionEffectContainer functionality at the InterEffectSet level.
 * Used for beam hit handlers, because for some reason those don't use the segmentcontroller's main effect container,
 * but rather their own weird interface that has a different, duplicate implementation in every variant of segmentcontroller...
 */
public class BastionConditionalInterEffectSet extends InterEffectSet {
    private final BastionField bfield;
    private final SegmentController segmentController;

    /**
    @param segmentController the SegmentController protected by this InterEffectSet.
     */
    public BastionConditionalInterEffectSet(BastionField bfield, SegmentController segmentController) {
        this.bfield = bfield;
        this.segmentController = segmentController;
    }

    @Override
    public float getStrength(InterEffectHandler.InterEffectType interEffectType) {
        boolean bastionFieldActive = bfield.meetsOperationRequirements();
        boolean entityMeetsRequirements =
                (segmentController.railController.getRoot().getType() != SimpleTransformableSendableObject.EntityType.SHIP
                && segmentController.getConfigManager().isActive(bastionFieldStatusEffect)
                && !(segmentController.railController.isTurretDocked() && segmentController.isAIControlled()));
        if(bastionFieldActive && entityMeetsRequirements){
            return 1.0f;
        }
        else return super.getStrength(interEffectType);
    }

    @Override
    public String toString() {
        return "[BASTION-CONDITIONAL]" + super.toString();
    }
}
