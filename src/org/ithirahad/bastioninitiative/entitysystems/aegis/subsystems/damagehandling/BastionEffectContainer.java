package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.damagehandling;

import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.BastionField;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.HitReceiverType;
import org.schema.game.common.controller.damage.effects.InterEffectContainer;
import org.schema.game.common.controller.damage.effects.InterEffectHandler.InterEffectType;
import org.schema.game.common.controller.damage.effects.InterEffectSet;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;

import static org.ithirahad.bastioninitiative.BIStatusEffectManager.bastionFieldStatusEffect;
import static org.schema.game.common.controller.damage.effects.InterEffectHandler.InterEffectType.*;

/**
Copied and modified from SegmentControllerEffectContainer.
 */
public class BastionEffectContainer extends InterEffectContainer {
    private final BastionField bfield;
    private final SegmentController segmentController;
    private final InterEffectSet invincible;
    public BastionEffectContainer(BastionField bastionField, SegmentController containing) {
        bfield = bastionField;
        this.segmentController = containing;
        invincible = new InterEffectSet(){
            {
                setAllStrengths();
            }

            @Override
            public float getStrength(InterEffectType var1){
                return 1.0f; //1.0 should be fine of course... but we may need some FP margin idk
            }

            @Override
            public boolean hasEffect(InterEffectType interEffectType) {
                return true;
            }

            @Override
            public void reset() {
                setAllStrengths();
            }

            private void setAllStrengths(){
                setStrength(EM,1);
                setStrength(HEAT,1);
                setStrength(KIN,1);
            }

            @Override
            public void setDefenseFromInfo(ElementInformation elementInformation) {
                //ignore that
                setAllStrengths();
            }
        };
    }

    @Override
    public InterEffectSet[] setupEffectSets() {
        InterEffectSet[] var1 = new InterEffectSet[3];

        for(int var2 = 0; var2 < var1.length; ++var2) {
            var1[var2] = new InterEffectSet();
        }

        return var1;
    }

    @Override
    public InterEffectSet get(HitReceiverType var1) {
        if (var1 != HitReceiverType.BLOCK && var1 != HitReceiverType.SHIELD && var1 != HitReceiverType.ARMOR) {
            throw new RuntimeException("illegal hit received " + var1.name());
        } else {
            boolean bastionFieldActive = bfield.meetsOperationRequirements();
            boolean entityMeetsRequirements =
                    (segmentController.railController.getRoot().getType() != SimpleTransformableSendableObject.EntityType.SHIP
                            && segmentController.getConfigManager().isActive(bastionFieldStatusEffect)
                            && !(segmentController.railController.isTurretDocked() && segmentController.isAIControlled()));
            if(var1 != HitReceiverType.SHIELD && bastionFieldActive && entityMeetsRequirements){
                return invincible;
            } else if (var1 == HitReceiverType.BLOCK) {
                return this.sets[0];
            } else {
                return var1 == HitReceiverType.ARMOR ? this.sets[1] : this.sets[2];
            }
        }
    }

    @Override
    public void update(ConfigEntityManager var1) {
        this.update(var1, HitReceiverType.BLOCK);
        this.update(var1, HitReceiverType.SHIELD);
        this.update(var1, HitReceiverType.ARMOR);
    }

    private void update(ConfigEntityManager var1, HitReceiverType var2) {
        InterEffectSet var3 = this.get(var2);
        this.addGeneral(var1, var3);
        if (var2 == HitReceiverType.ARMOR) {
            this.addArmor(var1, var3);
        } else {
            if (var2 == HitReceiverType.SHIELD) {
                this.addShield(var1, var3);
            }

        }
    }
}
