package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.network.packets.PacketUtil;
import org.ithirahad.bastioninitiative.BIConfiguration;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisSubsystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.damagehandling.BastionConditionalInterEffectSet;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.damagehandling.BastionEffectContainer;
import org.ithirahad.bastioninitiative.network.DisruptionHUDUpdatePacket;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.damage.beam.DamageBeamHitHandlerSegmentController;
import org.schema.game.common.controller.damage.beam.DamageBeamHittable;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.rails.RailController;
import org.schema.game.common.data.blockeffects.config.ConfigEntityManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.aegisInterference;
import static org.ithirahad.bastioninitiative.BIStatusEffectManager.bastionFieldStatusEffect;
import static org.ithirahad.bastioninitiative.util.BIUtils.getSelfAndAllDocks;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.SHIP;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class BastionField extends AegisSubsystem {
    //TODO: rename? "warding field"? Current name conflicts with bastion shielding chamber
    private static final float EPSILON = 0.00000001f;
    private float rawDisruptionDamage; //damage accumulated from incoming beams
    private boolean lastRemoteActiveStatus;
    private transient List<SegmentController> tmpToApply = new LinkedList<>();
    private transient Set<SegmentController> applied = new HashSet<>();
    private static HashMap<String,Long> notificationTimeTracker = new HashMap<>();
    private transient long lastInvulnMessageTime = 0;
    private transient long lastDamageUpdate = 0;
    private double lastRemoteEffectiveStructure = 100.0d;
    private boolean needsToUpdateClients = false;

    public BastionField(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, elementEntries.get("Aegis Invulnerability").id);
        lastDamageUpdate = now();
    }

    @Override
    protected void onServerBlockLinkedOrLoadedLinked() {

    }

    @Override
    protected void onServerBlockUnlinked() {

    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        return BASTION_FIELD_FUEL_BASE +
                Math.round(structPoints * BIConfiguration.BASTION_FIELD_FUEL_PER_STRUCTURE_POINT) +
                Math.round(getEnhancement() * BASTION_FIELD_FUEL_PER_ENHANCER);
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return BIConfiguration.BASTION_FIELD_FUEL_TO_ONLINE_BASE + Math.round(structPoints * BIConfiguration.BASTION_FIELD_ONLINING_COST_PER_STRUCTURE_POINT);
    }

    @Override
    public double getPowerConsumptionActive() {
        return BASTION_FIELD_ADDITIVE_POWER + (segmentController.getMassWithDocks() * BASTION_FIELD_POWER_PER_MASS);
        //actually using mass here makes sense imho as it require some investment to e.g. protect a planet/plate, though it shouldn't cost that much.
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return super.getPowerConsumerCategory();
    }

    @Override
    public void handleServer(Timer timer, boolean b) {
        boolean currentActiveStatus = meetsOperationRequirements();
        if(currentActiveStatus != lastRemoteActiveStatus){
            lastRemoteActiveStatus = currentActiveStatus;
            needsToUpdateClients = true;
        }

        handleCommon(timer,b);

        if(getParentCore() != null && !blocks.isEmpty()){
            long now = now();
            long delta = now-lastDamageUpdate;
            if(delta >= 1000) { //check every second, discretely.
                // This sidesteps any accuracy issues with a real-time DPS limiter and small numbers, while also giving the appearance of a
                // computer's loading or progress bar, which is actually quite thematic for a beam based on "Hollywood hacking".
                double deltaSeconds = delta * 0.001;
                double maxDamage = (DISRUPTOR_MAX_FRACTION_PER_SECOND * getParentCore().getMaxHP() * deltaSeconds); //greatest amount of disruption DPS possible
                if (rawDisruptionDamage > maxDamage)
                    rawDisruptionDamage = (float) maxDamage;
                AegisCore core = getParentCore();
                if (core != null) {
                    double maxHP = core.getMaxHP();
                    core.doDisruptionDamage((float) (rawDisruptionDamage * (maxHP/(getEffectiveStructPointsForDisruptor() * DISRUPTION_HP_PER_STRUCTURE_POINT)))); //reduce proportionally to raw HP/EHP ratio
                }
                rawDisruptionDamage = 0;
                lastDamageUpdate = now;
            }
        }

        if(needsToUpdateClients){
            syncToNearbyClients();
            needsToUpdateClients = false;
        }
    }

    @Override
    protected void handleClient(Timer timer, boolean hasBlocks) {
        super.handleClient(timer, hasBlocks);
        handleCommon(timer,hasBlocks);
    }

    private void handleCommon(Timer timer, boolean b) {
        boolean activated = getPowered() >= 1.0f - EPSILON && meetsOperationRequirements();

        tmpToApply.clear();
        tmpToApply.add(segmentController);

        getSelfAndAllDocks(segmentController, tmpToApply, true);

        applyDamageHandlingChangesToNewEntities(tmpToApply);

        for(SegmentController s : tmpToApply){
            ConfigEntityManager cem = s.getConfigManager();
            if (activated && (!s.getDockingController().isTurretDocking() || !s.isAIControlled())) {
                if (!cem.isActive(bastionFieldStatusEffect))
                    cem.addEffectAndSend(bastionFieldStatusEffect, true, s.getNetworkObject());
            } else if (cem.isActive(bastionFieldStatusEffect))
                cem.removeEffectAndSend(bastionFieldStatusEffect, true, s.getNetworkObject());
        }
        tmpToApply.clear();
        //add/remove effect as necessary
    }

    private void applyDamageHandlingChangesToNewEntities(Collection<SegmentController> allEntities) {
        for(SegmentController s : allEntities) if(!applied.contains(s)){
            MiscUtils.setSuperclassPrivateField(SimpleTransformableSendableObject.class, "effectContainer", s, new BastionEffectContainer(this, s)); //this one is only for cannons apparently; maybe missiles too but doesn't work
            if(s instanceof DamageBeamHittable){
                DamageBeamHitHandlerSegmentController dbh = (DamageBeamHitHandlerSegmentController) ((DamageBeamHittable) s).getDamageBeamHitHandler();
                //probably has to be this class since it's a segcon
                Field[] fields = dbh.getClass().getDeclaredFields();
                List<Field> toReplace = new LinkedList<>();
                toReplace.add(fields[2]);
                toReplace.add(fields[3]);
                toReplace.add(fields[10]);
                //these are the armour and block defenses. Couldn't get them by string for whatever reason.
                try {
                    for(Field fd : toReplace){
                        fd.setAccessible(true);
                        fd.set(dbh,new BastionConditionalInterEffectSet(this,s));
                        fd.setAccessible(false);
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            applied.add(s);
        }
        //TODO: If planet, check core
    }

    public boolean meetsOperationRequirements() {
        if(isOnServer()) return (segmentController.isFullyLoaded() && isPresentAndLinked() && getParentCore() != null && getParentCore().getRemoteStatus() != VirtualAegisSystem.AegisSystemStatus.VULNERABLE && isSuppliedAegisCharge() && !segmentController.getConfigManager().isActive(aegisInterference));
        else return lastRemoteActiveStatus;
    }

    public double getEffectiveStructPointsForDisruptor(){
        double v;
        if(isOnServer()) {
            AegisCore parent = getParentCore();
            if (parent != null) if (!blocks.isEmpty()) {
                double fromEnhancement = getEnhancement() * BASTION_FIELD_EFFECTIVE_STRUCTURE_POINTS_PER_ENHANCER;
                v = getParentCore().getStructurePoints(false) + fromEnhancement;
            } else v = getParentCore().getStructurePoints(false);
            else v = 100.0d;

            if(lastRemoteEffectiveStructure != v) needsToUpdateClients = true;
            lastRemoteEffectiveStructure = v;
        }
        return lastRemoteEffectiveStructure;
    }

    @Override
    public void onSerialize(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(lastRemoteActiveStatus);
        b.writeDouble(lastRemoteEffectiveStructure);
        b.writeFloat(rawDisruptionDamage);
    }

    @Override
    public void onDeserialize(PacketReadBuffer b) throws IOException {
        lastRemoteActiveStatus = b.readBoolean();
        lastRemoteEffectiveStructure = b.readDouble();
        rawDisruptionDamage = b.readFloat();

        if(tmpToApply == null) tmpToApply = new ArrayList<>();
        if(applied == null) applied = new HashSet<>();
    }

    @Override
    public String getShortPurposeString() {
        return "Grants invulnerability to the structure, docked ships, and inactive turrets";
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Bastion Field Subsystem";
    }

    public void sendInvulnerabilityMessageIfReady(PlayerState playerState) {
        if (isOnServer()) {
            long now = now();
            String player = playerState.toString();
            if (!notificationTimeTracker.containsKey(player) || notificationTimeTracker.get(player) > MIN_INVULN_MESSAGE_INTERVAL_MS) {
                playerState.sendServerMessage(Lng.astr(segmentController.getRealName() + " is protected by a Bastion Field! Its blocks are invulnerable to weapon damage while its Aegis System is active."), MESSAGE_TYPE_INFO);
                notificationTimeTracker.put(player,now);
            }
        }
    }

    public void doDisruptionDamage(float power, int factionId) {
        if(lastRemoteActiveStatus) {
            rawDisruptionDamage += power;
            getParentCore().setLastHitter(factionId);
        }
    }

    public void sendDisruptionUIInfo(SegmentController beamShooter) {
        AegisCore parent = getParentCore();
        if(parent != null && isOnServer()) {
            Set<PlayerState> players = new HashSet<>(((ManagedUsableSegmentController<?>) beamShooter).getAttachedPlayers());
            players.addAll(((ManagedUsableSegmentController<?>) segmentController).getAttachedPlayers());
            for (PlayerState player : players) {
                PacketUtil.sendPacket(player, new DisruptionHUDUpdatePacket(parent.getCurrentHP(), parent.getMaxHP(), parent.canDisrupt()));
            }
        }
    }

    public static boolean isSegmentControllerInvulnerable(SegmentController sc) {
        if(sc == null) return false;
        RailController rdc = sc.railController;
        if(rdc.getRoot().getType() == SHIP) return false;
        if (sc instanceof ManagedUsableSegmentController<?> && !(rdc.isTurretDocked() && sc.isAIControlled()))
        {
            final short bastionFieldID = elementEntries.get("Aegis Invulnerability").id;
            ManagedUsableSegmentController<?> rootMSC = (ManagedUsableSegmentController<?>) rdc.getRoot();
            BastionField bsf = (BastionField) rootMSC.getManagerContainer().getModMCModule(bastionFieldID);
            return bsf.meetsOperationRequirements(); //na, nanana na na na, ...can't touch this!
        }
        return false;
    }

    public boolean getCachedActiveStatus() {
        return lastRemoteActiveStatus;
    }
}
