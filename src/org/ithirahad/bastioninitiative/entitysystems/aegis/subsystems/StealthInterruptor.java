package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems;

import api.common.GameCommon;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.sound.AudioUtils;
import org.apache.poi.util.NotImplemented;
import org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem;
import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisSubsystem;
import org.ithirahad.bastioninitiative.vfx.particle.DecloakParticleEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.cloaking.StealthAddOn;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

import javax.vecmath.Vector3f;

import java.util.ArrayList;

import static com.google.common.primitives.Floats.max;
import static java.lang.Math.round;
import static org.ithirahad.bastioninitiative.BIConfiguration.*;
import static org.ithirahad.bastioninitiative.persistence.VirtualAegisSystem.AegisSystemStatus.DISABLED;
import static org.ithirahad.bastioninitiative.util.BIUtils.isSelfOrAlly;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.getServerSendables;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_INFO;

public class StealthInterruptor extends AegisSubsystem {
    private static long VISUAL_HEAD_START_MS = 0;
    /**
    The fraction of the activation interval when the visual should be played.
     */
    private static final double VISUAL_HEAD_START_FRACTION = 0.75f;
    long lastActivation = 0;

    //TODO: Interruptor detection chamber in short range scan tree, with a sphere WorldDrawer for radii.
    // I guess instead of wrangling with textures and UV fuckery, the shader can just determine the transparency
    // based on the normal's proximity to the nearest whole-number xyz coordinate
    // (with a pretty hard ramp though, so it's mostly see through except very near the integer)
    public StealthInterruptor(SegmentController segmentController, ManagerContainer<?> managerContainer) {
        super(segmentController, managerContainer, elementEntries.get("Aegis Anti Stealth").id);
        updateVFXLeadTime();
    }

    public void updateVFXLeadTime(){
        VISUAL_HEAD_START_MS = Math.round(VISUAL_HEAD_START_FRACTION * AEGIS_ANTI_STEALTH_PULSE_INTERVAL_MS);
    }

    @Override
    public void handleServer(Timer timer, boolean hasBlock) {
        if(isPresentAndLinked()) {
            VirtualAegisSystem.AegisSystemStatus status = getParentCore().getVirtualSystem().getStatus();
            if (status != DISABLED && status != VirtualAegisSystem.AegisSystemStatus.NO_CHARGE) {
                long deltaT = timer.currentTime - lastActivation;
                int ownFaction = segmentController.getFactionId();
                Vector3i sector = segmentController.getSector(new Vector3i());
                int targetFaction = 0;
                if (deltaT > AEGIS_ANTI_STEALTH_PULSE_INTERVAL_MS) {
                    for (SimpleTransformableSendableObject<?> obj : segmentController.getRemoteSector().getServerSector().getEntities()) {
                        targetFaction = obj.getFactionId();
                        if (!isSelfOrAlly(targetFaction, ownFaction) && obj instanceof ManagedUsableSegmentController && isInRange(obj)) {
                            //it's a ship or something. Decloak it!
                            ManagedUsableSegmentController<?> sc = (ManagedUsableSegmentController<?>) obj;
                            if (sc.railController.previous == null) {
                                StealthAddOn stealthSys = ((ManagedUsableSegmentController<?>) obj).getManagerContainer().getStealthAddOn();
                                if (stealthSys.isActive()) {
                                    stealthSys.activation = null;
                                    stealthSys.sendChargeUpdate();

                                    ArrayList<PlayerState> playersAboardShip = new ArrayList<>(sc.getAttachedPlayers());
                                    if(!GameCommon.isDedicatedServer()) playersAboardShip = getServerSendables(playersAboardShip);
                                    for (PlayerState playerAboard : playersAboardShip) {
                                        playerAboard.sendServerMessage(Lng.astr("Your concealment has been broken by the Aegis System aboard the " + segmentController.getRealName() + "!"), MESSAGE_TYPE_INFO);
                                    }
                                    AudioUtils.serverPlaySound("0022_item - forcefield powerdown", 1.0f, 1.0f, playersAboardShip); //TODO: custom effect would be better here
                                    AudioUtils.serverPlaySound("0022_item - shield activate", 1.0f, 1.0f, playersAboardShip);

                                    ArrayList<PlayerState> playersOnStation = new ArrayList<>(((ManagedUsableSegmentController<?>) segmentController).getAttachedPlayers());
                                    if(!GameCommon.isDedicatedServer()) playersOnStation = getServerSendables(playersOnStation);
                                    for (PlayerState playerHere : playersOnStation) {
                                        playerHere.sendServerMessage(Lng.astr(sc.getRealName() + "'s stealth system has been disrupted by the Aegis Stealth Interruptor!"), MESSAGE_TYPE_INFO);
                                    }
                                    AudioUtils.serverPlaySound("0022_gameplay - cockpit warning beep", 1.0f, 1.0f, playersOnStation);

                                    DecloakParticleEffect.FireEffectServer(segmentController.getSectorId(), sector, sc.getWorldTransform().origin, sc.getLinearVelocity(new Vector3f()), (int) max(sc.getBoundingBox().sizeX(), sc.getBoundingBox().sizeY(), sc.getBoundingBox().sizeZ()));
                                    //TODO sound/VFX on target
                                    //TODO activate adjacent activators/buttons
                                }
                            }
                        }
                    }
                    lastActivation = timer.currentTime;
                } else if (deltaT > AEGIS_ANTI_STEALTH_PULSE_INTERVAL_MS - VISUAL_HEAD_START_MS) {
                    //TODO play sound & visual around station
                }
            }
        }
    }

    private boolean isInRange(SimpleTransformableSendableObject<?> obj) {
        //TODO: Adjust for position of Aegis Core

        Vector3f targPos = obj.getWorldTransform().origin;
        Vector3f thisPos = blocks.isEmpty()? segmentController.getWorldTransform().origin : segmentController.getSegmentBuffer().getPointUnsave(getBlock()).getWorldPos(new Vector3f(),segmentController.getSectorId());
        Vector3f delta = new Vector3f(thisPos);
        delta.sub(targPos);
        float dsquared = delta.lengthSquared();
        return dsquared < getMaxRangeSquared();
    }

    private float getMaxRangeSquared() {
        float r = getMaxRange();
        return r*r;
    }

    @NotImplemented //relies on enhancement
    private float getMaxRange() {
        return AEGIS_ANTI_STEALTH_BASE_RANGE + (getEnhancement() * AEGIS_ANTI_STEALTH_RANGE_PER_ENHANCER);
    }

    @Override
    public void onSerialize(PacketWriteBuffer b){
    }

    @Override
    public void onDeserialize(PacketReadBuffer b){
        updateVFXLeadTime();
    }

    @Override
    protected void onServerBlockLinkedOrLoadedLinked() {
        //nothing to do here really
    }

    @Override
    protected void onServerBlockUnlinked() {

    }

    @Override
    public int getAegisChargeConsumptionPerDay(float structPoints) {
        return AEGIS_ANTI_STEALTH_BASE_FUEL_CONS + round(getEnhancement() * AEGIS_ANTI_STEALTH_FUEL_PER_ENHANCER);
    }

    @Override
    public int getAegisChargeConsumptionToPutOnline(float structPoints) {
        return 1;
    }

    @Override
    public double getPowerConsumptionActive() {
        return STEALTH_BREAKER_FLAT_POWER_REQ + (getEnhancement() * STEALTH_BREAKER_POWER_PER_ENHANCER);
    }

    @Override
    public String getName() {
        return ElementKeyMap.getInfo(getBlockId()).getName();
    }

    @Override
    public String getFullSystemName() {
        return "Aegis Stealth Interruption Subsystem";
    }

    @Override
    public String getShortPurposeString() {
        return "Emits pulses that disable the stealth systems of non-allied ships within range.";
    }
}
