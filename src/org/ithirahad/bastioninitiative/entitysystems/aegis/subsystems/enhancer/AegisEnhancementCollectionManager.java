package org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer;

import org.ithirahad.bastioninitiative.entitysystems.aegis.AegisCore;
import org.schema.game.client.view.gui.structurecontrol.GUIKeyValueEntry;
import org.schema.game.client.view.gui.structurecontrol.ModuleValueEntry;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.power.reactor.PowerConsumer;
import org.schema.game.common.data.SegmentPiece;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.Timer;

public class AegisEnhancementCollectionManager extends ControlBlockElementCollectionManager<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> implements PowerConsumer {

    private AegisCore core;
    private int enhancement = 0;
    private float powered;
    private boolean flagCollectionChanged = true;

    public AegisEnhancementCollectionManager(short enhancerID, SegmentPiece element, SegmentController segmentController, AegisEnhancementElementManager aem) {
        super(element, enhancerID, segmentController, aem);
    }

    public void setCore(AegisCore v){
        core = v;
    }

    @Override
    public boolean isUsingIntegrity() {
        return false;
    } // f that

    @Override
    public int getMargin() {
        return 0;
    }

    @Override
    protected Class<AegisEnhancerUnit> getType() {
        return AegisEnhancerUnit.class;
    }

    @Override
    public boolean needsUpdate() {
        return true;
    }

    @Override
    public void update(Timer timer) {
        super.update(timer);
        refreshEnhancement();
    }

    @Override
    public AegisEnhancerUnit getInstance() {
        return new AegisEnhancerUnit();
    }

    @Override
    public void onChangedCollection() {
        super.onChangedCollection();
        flagCollectionChanged = true;
    }

    @Override
    public String getModuleName() {
        return getControllerElement().getInfo().name;
    } //conveniently, those are all named appropriately

    @Override
    public GUIKeyValueEntry[] getGUICollectionStats() {
        return new GUIKeyValueEntry[]{
                new ModuleValueEntry(Lng.str("Subsystem Enhancement Level"), enhancement),
        };
    }

    @Override
    public double getPowerConsumedPerSecondResting() {
        return 0; //handled by subsystem MMCM (different systems use different amounts of power per enhancer)
    }

    @Override
    public double getPowerConsumedPerSecondCharging() {
        return 0; //handled by subsystem MMCM (different systems use different amounts of power per enhancer)
    }

    @Override
    public boolean isPowerCharging(long l) {
        return getControllerElement().isActive();
    }

    @Override
    public void setPowered(float powered) {
        this.powered = powered;
    }

    @Override
    public float getPowered() {
        return powered;
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return null;
    }

    @Override
    public void reloadFromReactor(double v, Timer timer, float v1, boolean b, float v2) {

    }

    @Override
    public boolean isPowerConsumerActive() {
        return (core != null && core.isPowerConsumerActive());
    }

    @Override
    public void dischargeFully() {

    }

    public void addEnhancement(int enhancement) {
        this.enhancement += enhancement;
    }

    public void refreshEnhancement() {
        enhancement = 0; //default (all subsystems have a base value + (enhancement * addlValuePerEnhancer))
        for (AegisEnhancerUnit w : getElementCollections()) {
            w.refreshSubsysCapabilities(this);
        }
    }

    public int getEnhancement() {
        return enhancement;
    }
}
