This code is adapted from StarMade's source, specifically the factory+factory enhancer system code.
As such I can't fairly say that any of it is even "my own" per se.
I may or may not know how any of it works; I just hope that it actually does work in the end.

Notably, unlike with factories where everything is integrated, this code only handles enhancers.
The behavior of the subsystems themselves (including consuming power) is handled by the MMCMs.
