package org.ithirahad.bastioninitiative.entitysystems.aegis;

import api.mod.StarMod;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import api.utils.game.module.ModManagerContainerModule;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.ithirahad.bastioninitiative.BastionInitiative;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementCollectionManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancementElementManager;
import org.ithirahad.bastioninitiative.entitysystems.aegis.subsystems.enhancer.AegisEnhancerUnit;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.ManagerModuleCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.schine.graphicsengine.core.Timer;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.RejectedExecutionException;

import static org.ithirahad.bastioninitiative.util.BIUtils.findLinkedBlocks;
import static org.ithirahad.bastioninitiative.util.TemporalShortcuts.now;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;

public abstract class AegisSubsystem extends ModManagerContainerModule {
    private static final long SUBSYS_FORCE_UPDATE_INTERVAL = 500; //remove?
    private long lastUpdate = now(); //remove?

    private static final double POWER_EPSILON = 0.0001;
    protected transient boolean loaded = false; //taking advantage of how deserialized things get their transient fields reset to default, this will always come up false when needed
    private transient boolean didFirstConnectCheck = false;
    protected boolean initialized = false;
    protected boolean isSuppliedCharge = false;
    private boolean presentAndLinkedRemote = false;
    private boolean acceptingPower = true;
    private int lastRemoteEnhancement = 0;
    private Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> linksFromCore;

    public AegisSubsystem(SegmentController segmentController, ManagerContainer<?> managerContainer, StarMod modInstance, short id) {
        super(segmentController, managerContainer, modInstance, id);
        onLoadGeneric();
    }
    /**
     Creates a new Aegis Subsystem MMCM with Bastion Initiative as the owning mod. Not recommended for extensions' subsystems.
     */
    public AegisSubsystem(SegmentController segmentController, ManagerContainer<?> managerContainer, short i) {
        this(segmentController, managerContainer, BastionInitiative.modInstance, i);
    }

    private void onLoadGeneric() {
        //setBlockSpecialInContainer(getManagerContainer(), getBlockId());
        onLoad();
        loaded = true;
    }

    protected void onLoad() {
        //to inherit
    }

    public final boolean isPresentAndLinked(){
        return presentAndLinkedRemote;
    }

    public final void updatePresentAndLinked(){
        if(isOnServer()) {
            AegisCore parent = getParentCore();
            if (parent == null ||
                !parent.isInitialized() ||
                blocks.isEmpty() ||
                parent.blocks.isEmpty()) {
                setPresentAndLinkedOnServer(false);
            }
            else {
                linksFromCore = findLinkedBlocks(segmentController, parent.getBlock());
                boolean b = (linksFromCore.containsKey(getBlockId()) && !linksFromCore.get(getBlockId()).isEmpty());
                if(b != presentAndLinkedRemote){
                    setPresentAndLinkedOnServer(b);
                    syncToNearbyClients();
                }
            }
        }
    }

    private void setPresentAndLinkedOnServer(boolean b) {
        boolean isGuaranteedActualChange = segmentController.isFullyLoaded() && !segmentController.getRemoteSector().isWrittenForUnload();
        //filter out load/unload related stuff
        if(presentAndLinkedRemote != b && isGuaranteedActualChange && getParentCore() != null){
            if(!didFirstConnectCheck) didFirstConnectCheck = true;
            else getParentCore().onSubsystemLinkageChanged();
        }
        if(!presentAndLinkedRemote && b){
            onServerBlockLinkedOrLoadedLinked();
        }
        else if(presentAndLinkedRemote && !b && isGuaranteedActualChange){
            onServerBlockUnlinked();
        }
        presentAndLinkedRemote = b;
    }

    public final boolean isSuppliedAegisCharge(){
        return isSuppliedCharge;
    };

    @Override
    public final void handle(Timer timer) {
        if(segmentController.isFullyLoaded()) {
            if (isOnServer()) {
                //if (now() > lastUpdate + SUBSYS_FORCE_UPDATE_INTERVAL) {
                updatePresentAndLinked();
                getEnhancement();
                //lastUpdate = now();
                //}
                handleServer(timer, !blocks.isEmpty());
            }
            else handleClient(timer, !blocks.isEmpty());
        }
    }

    protected abstract void handleServer(Timer timer, boolean hasBlocks);

    protected void handleClient(Timer timer, boolean hasBlocks){
        //most systems won't need this, but I guess for VFX or whatever
    }

    public final long getBlock(){
        if(!this.blocks.isEmpty()) return (long) blocks.keySet().toArray()[0];
        else throw new IllegalStateException("Attempted to retrieve aegis subsystem block when aegis subsystem does not have a block.");
    }

    public final int getEnhancement(){
        if(isOnServer()) updateEnhancementOnServer();
        return lastRemoteEnhancement;
    }

    private void updateEnhancementOnServer() {
        if(!blocks.isEmpty()) {
            float v;
            v = getEnhancementRaw(getBlock());
            if (v != lastRemoteEnhancement) v = lastRemoteEnhancement;
        } else lastRemoteEnhancement = 0;
    }

    public final AegisCore getParentCore(){
        ModManagerContainerModule mom = getManagerContainer().getModMCModule(elementEntries.get("Aegis Core").id);
        if(mom instanceof AegisCore) return (AegisCore) mom;
        else return null;
    }

    public final ManagerModuleCollection<AegisEnhancerUnit, AegisEnhancementCollectionManager, AegisEnhancementElementManager> getEnhancerModuleCollection(){
        AegisCore c = getParentCore();
        if(c == null) return null;
        return c.subsystemEnhancerManagerMap.get(getBlockId());
    }

    @Override
    public boolean isPowerCharging(long l) {
        return acceptingPower && isPresentAndLinked() && isSuppliedAegisCharge();
        //here, "charging" essentially means "functional".
    }

    public final boolean isPowered(){
        return isPowerCharging(getBlock()) && getPowered() > 1-POWER_EPSILON;
    }

    @Override
    public final boolean isPowerConsumerActive() {
        return isPowerCharging(0L); //abs. pos is just whatever here really
    }

    @Override
    public PowerConsumerCategory getPowerConsumerCategory() {
        return PowerConsumerCategory.OTHERS;
    }

    public final double getPowerConsumedPerSecondCharging(){
        if(isPresentAndLinked() && acceptingPower) return
                getPowerConsumptionActive();
        else return 0;
    }

    @Override
    public final double getPowerConsumedPerSecondResting() {
        return 0;
    }

    @Override
    public final void onTagSerialize(PacketWriteBuffer b) throws IOException {
        b.writeBoolean(presentAndLinkedRemote);
        b.writeBoolean(acceptingPower);
        b.writeBoolean(isSuppliedCharge);
        b.writeInt(lastRemoteEnhancement);
        b.writeBoolean(acceptingPower);
        onSerialize(b);
    }

    protected abstract void onSerialize(PacketWriteBuffer b) throws IOException;

    @Override
    public final void onTagDeserialize(PacketReadBuffer b) throws IOException {
        if(!loaded) onLoadGeneric();
        if(isOnServer()){
            boolean presentAndLinkedRecieved = b.readBoolean();
            if(presentAndLinkedRemote != presentAndLinkedRecieved) updatePresentAndLinked();
            //have to avoid infinite loop
        }
        else presentAndLinkedRemote = b.readBoolean();
        acceptingPower = b.readBoolean();
        isSuppliedCharge = b.readBoolean();
        lastRemoteEnhancement = b.readInt();
        acceptingPower = b.readBoolean();
        onDeserialize(b);
    }

    protected abstract void onDeserialize(PacketReadBuffer b) throws IOException;


    @Override
    public final void handlePlace(long l, byte b) {
        super.handlePlace(l, b);
        //The thing needs to be linked.
        //Just placing an Aegis Subsystem block doesn't do anything, so no Aegis System should be specifying any behaviour here.
    }

    @Override
    public final void handleRemove(long l) {
        super.handleRemove(l);
        updatePresentAndLinked();
    }

    protected final void setAcceptingPower(boolean v){
        if(isOnServer() && v != acceptingPower) {
            acceptingPower = v;
            syncToNearbyClients();
        } else throw new RejectedExecutionException("Attempted to set server value on client!");
    }

    @Override
    public String getName() {
        return ElementKeyMap.getInfo(getBlockId()).getName();
    }

    private int getEnhancementRaw(long abs){
        if(isPresentAndLinked()) {
            if (!blocks.isEmpty()) {
                if (blocks.containsKey(abs)) {
                    Long2ObjectMap<AegisEnhancementCollectionManager> cms = Objects.requireNonNull(getEnhancerModuleCollection()).getCollectionManagersMap();
                    AegisEnhancementCollectionManager cm = cms.get(abs);
                    return cm.getEnhancement();
                } else throw new IllegalArgumentException("Absolute index does not contain a matching type! It may have been retrieved using the wrong message.");
            }
        }
        return 0;
    }

    public abstract String getShortPurposeString();

    protected abstract void onServerBlockLinkedOrLoadedLinked();

    protected abstract void onServerBlockUnlinked();

    public abstract int getAegisChargeConsumptionPerDay(float structPoints); //determiner is mass for now; more like abstract "value points" later
    public abstract int getAegisChargeConsumptionToPutOnline(float structPoints);

    public abstract double getPowerConsumptionActive();

    public final void setSuppliedAegisCharge(boolean val){
        isSuppliedCharge = val;
    };
    public abstract String getFullSystemName();
    public void onDeInitialize() {
    }
}
