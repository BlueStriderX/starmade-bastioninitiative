package org.ithirahad.bastioninitiative;
import static org.ithirahad.bastioninitiative.util.Constants.MS_TO_MINUTES;

public class BIConfiguration {
    public static float DISRUPTION_HP_PER_STRUCTURE_POINT = 1.0f;
    public static int DISRUPTION_BASE_DURATION_MS = MS_TO_MINUTES*30;
    public static long MIN_INVULN_MESSAGE_INTERVAL_MS = 1500;

    public static long ALERT_NOTIFICATION_INTERVAL_MS = 2000;

    public static float AEGIS_CORE_BASE_POWER_PER_STRUCTURE_POINT = 0.1f;

    public static float BASTION_FIELD_POWER_PER_MASS = 1f;
    public static float BASTION_FIELD_ADDITIVE_POWER = 1500f;

    public static float AEGIS_ONLINING_COST_PER_STRUCTURE_POINT = 0f;
    public static int AEGIS_ONLINING_COST_BASE = 100;

    public static int BASTION_FIELD_FUEL_BASE = 0;
    public static float BASTION_FIELD_FUEL_PER_STRUCTURE_POINT = 0.1f;
    public static float BASTION_FIELD_FUEL_PER_ENHANCER = 0.5f;
    public static float BASTION_FIELD_EFFECTIVE_STRUCTURE_POINTS_PER_ENHANCER = 10f;
    //TODO: when structure points are separated from mass, make sure this is considerably more than adding dummy blocks
    //effective extra structure points per enhancer, for calculating hack rate

    public static int BASTION_FIELD_FUEL_TO_ONLINE_BASE = 1000;
    public static float BASTION_FIELD_ONLINING_COST_PER_STRUCTURE_POINT = 1f;

    public static int STELLAR_NEXUS_ONLINING_COST = 100000; //flat cost required to activate a Stellar Nexus
    public static int STELLAR_NEXUS_FUEL_BASE = 200;
    public static float STELLAR_NEXUS_FUEL_PER_LINKED_CONSUMPTION = 0.01f; //fuel cost per Aegis Charge consumption of linked systems
    public static double STELLAR_NEXUS_POWER_CONSUMPTION = 10000; //TODO: is there any better solution than this hard minimum on station size?

    public static final int AEGIS_TRANSMITTER_FUEL_CONSUMPTION_BASE = 10;
    public static float AEGIS_TRANSMITTER_FUEL_CONSUMPTION_PER_CAPABILITY = 0f; //basically inefficiency - fuel consumed per fuel sent
    public static float AEGIS_TRANSMITTER_FUEL_CONSUMPTION_PER_DISTANCE_SECTORS = 0f; //extra fuel lost per fuel transmitted per sector traveled

    public static int AEGIS_TRANSMITTER_FUEL_CONSUMPTION_ONLINING_BASE = 1;

    public static long AEGIS_ANTI_STEALTH_PULSE_INTERVAL_MS = 10000; //time between anti stealth pulses
    public static float AEGIS_ANTI_STEALTH_BASE_RANGE = 100f;
    public static float AEGIS_ANTI_STEALTH_RANGE_PER_ENHANCER = 5f;

    public static int AEGIS_ANTI_STEALTH_BASE_FUEL_CONS = 10;
    public static float AEGIS_ANTI_STEALTH_FUEL_PER_ENHANCER = 1;

    public static float AEGIS_ANTI_FTL_RANGE_BASE = 1.5f;
    public static float AEGIS_ANTI_FTL_RANGE_PER_ENHANCER = 0.5f;
    public static float AEGIS_ANTI_FTL_RANGE_WARPSPACE_MODIFIER_PER_WARP_FACTOR = 1f; //normally range per enhancer will simply be divided by warp factor; this lets you modify it

    public static int AEGIS_ANTI_FTL_FUEL_CONSUMPTION_ONLINING_BASE = 10000; //turning these on and off should not be a sustainable strategy for saving fuel
    public static int AEGIS_ANTI_FTL_BASE_FUEL_CONS = 500;
    public static float AEGIS_ANTI_FTL_FUEL_PER_ENHANCER = 10f;
    public static float AEGIS_ANTI_FTL_BASE_POWER_CONS = 1000f;
    public static float AEGIS_ANTI_FTL_POWER_PER_ENHANCER = 10f;

    public static int AEGIS_RECYCLE_TIME_CHANGE_COOLDOWN_DAYS = 1; //cycles that must elapse between reset time changes
    public static int DISRUPTION_SAFETY_DAYS = 0; //minimum number of days between siege attempts
    public static float BASTION_SHIELD_AMPLIFICATION_FACTOR = 20.0f; //shield capacity multiplier for chamber
    public static float BASTION_WEAPON_RANGE_FACTOR = 2.5f; //weapon range multiplier from chamber

    public static float STEALTH_BREAKER_FLAT_POWER_REQ = 1000.0f; //flat power consumption of stealth-breaker system
    public static float STEALTH_BREAKER_POWER_PER_ENHANCER = 10f; //enhancers boost range; this is the extra power needed

    public static int AEGIS_MIN_DOWNTIME_MINUTES = 30; //minimum disruption time
    public static float AEGIS_ADDL_MINUTES_DOWNTIME_PER_MASS = 0.066666666666f; //added length of hacked vulnerability window per mass
    public static float AEGIS_DOWNTIME_REDUCTION_PER_ENHANCER_EXP = 0.5f;
    public static float AEGIS_DOWNTIME_REDUCTION_PER_ENHANCER_MULT = 0.99f; //TODO: figure out dim. returns formula
    public static boolean BASTION_FIELD_PROTECTS_TURRETS = false;

    //TODO: enhancers/mass/effectiveness maths

    public static long AEGIS_CORE_COLLECTION_INTERVAL_MS = 1000L; //time between Aegis Core checks for aegis cells in linked inventories
    public static int AEGIS_CORE_COLLECTION_MAX_THROUGHPUT = -1; //max cells collected at a time; -1 for infinite
    public static int AEGIS_CORE_CELL_CAPACITY = -1; //max cells worth of energy stored
    // TODO: enhancers?
    //  Also this could be a faction-wide "level cap" that limits unloaded asset holdings depending on active population or sth :P
    public static float LOW_FUEL_ALERT_FACTOR = 2.0f; //if a system has this many days worth of fuel or less, send the faction a notification

    public static int CENTRIFUGE_REFINE_RATIO = 30; //how many <other stuff> per castellium

    public static float DISRUPTOR_POTENCY_PER_BLOCK = 0.5f; //Maximum amount of structure points (modified by bastion field enhancer count) per block for full-speed hacking
    public static int DISRUPTOR_FUEL_PER_CYCLE_PER_BLOCK = 1; //amount of Aegis Cells consumed per firing cycle
    public static float DISRUPTOR_RANGE = 0.3f; //In reference ranges, i.e. "sectors". Can't be too long; needs to be in short-range weapons range preferably.
    public static double DISRUPTOR_POWER_CONSUMPTION_CHARGING = 120f;
    public static double DISRUPTOR_POWER_CONSUMPTION_RESTING = 50f;
    public static double DISRUPTOR_MAX_FRACTION_PER_SECOND = 0.01; //fraction (out of 1) of total disruption HP that can be sapped every second.

    //-----------------

    public static float STRUCTURE_POINTS_PER_FACTORY_CAPABILITY = 0.05f;
    public static float STRUCTURE_POINTS_PER_CARGO_SPACE = 0.03f;
    public static float STRUCTURE_POINTS_PER_SHIELD_GEN = 0.5f;
    public static float STRUCTURE_POINTS_PER_SHIELD_CAP = 0.5f;
    public static float STRUCTURE_POINTS_PER_ACTIVE_WEAPON_BLOCK = 1f; //incl missile capacity
    public static float STRUCTURE_POINTS_PER_REACTOR_LEVEL = 1f;    //exactly what it says on the tin
    public static float STRUCTURE_POINTS_REACTOR_ADDITIONAL_MULT_PER_CHAMBER_UTILIZATION = 0.5f; //added 'duplicate' structure points per reactor level based on chamber capacity used
}
