package org.ithirahad.bastioninitiative.vfx.particle;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import org.ithirahad.bastioninitiative.network.HexBurstFXRemoteExecutePacket;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;

import static org.ithirahad.bastioninitiative.vfx.particle.ParticleEffectsManager.TX_BST_HEX;

public class BastionFieldEffects {
    private static final Vector4f ringColour = new Vector4f(0.3f,0.3f,0.75f,0.7f);
    protected static final Vector4f hexesColour = new Vector4f(0.3f,1f,0.95f,0.5f);
    private static final float placementAdjMult = 2.0f;

    public static void FireHexBallEffectClient(final int sectorId, final Vector3f position, final float scale, final Vector4f color, boolean isExplosion){
        final Vector3f offset = new Vector3f();
        Vector3f hitPos = new Vector3f(position);
        final Random rng = new Random();
        final float adjScale = isExplosion? Math.min(1+FastMath.sqrt(scale/20f),10f) : Math.min(0.5f + 0.5f*FastMath.sqrt(scale/30f),5f); //TODO tune
        final int particlesPerWave = isExplosion? 12 : 6;
        for(int i = 0; i < particlesPerWave; i++) {
            Vector3f pos = new Vector3f();
            offset.set(nextSignedFloat(rng), nextSignedFloat(rng), nextSignedFloat(rng));
            offset.normalize(); //random dir, not random radius
            offset.scale(adjScale * placementAdjMult);
            pos.set(hitPos);
            pos.add(offset);
            RadialHexParticle particle = new RadialHexParticle(hitPos,pos,adjScale);
            ModParticle.setColorF(particle, color);
            ModParticleUtil.playClient(sectorId, pos, TX_BST_HEX, particle);
        }

        new RealTimeDelayedAction(20, 1, false){
            @Override
            public void doAction() {
                for(int i = 0; i < particlesPerWave; i++) { //group of 8
                    Vector3f hitPos = new Vector3f(position);
                    Vector3f pos = new Vector3f();
                    offset.set(nextSignedFloat(rng), nextSignedFloat(rng), nextSignedFloat(rng));
                    offset.normalize(); //random dir, not random radius
                    offset.scale(adjScale * placementAdjMult);
                    pos.set(hitPos);
                    pos.add(offset);
                    RadialHexParticle particle = new RadialHexParticle(hitPos,pos,adjScale);
                    particle.lifetimeMs /= 2; //half the default
                    ModParticle.setColorF(particle, color);
                    ModParticleUtil.playClient(sectorId, pos, TX_BST_HEX, particle);
                }
            }
        }.startCountdown();
    }

    //Borrows the scanner ring effect from RRS, except in green and without forward motion.
    public static void FireEffectServer(final int sectorId, Vector3i sector, final Vector3f position, float scale, boolean isExplosion){
        //TODO Add inward offset pos as 'origin' for particles so they face more outwards

        //TODO FlatRingFXRemoteExecutePacket ringEffectPacket = new FlatRingFXRemoteExecutePacket(...);
        HexBurstFXRemoteExecutePacket onHitEffectPacket = new HexBurstFXRemoteExecutePacket(sectorId, position, scale, hexesColour, isExplosion);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4){
                //PacketUtil.sendPacket(player, ringEffectPacket);
                PacketUtil.sendPacket(player, onHitEffectPacket);
            }
        }
    }

    private static float nextSignedFloat(Random rng){
        return (rng.nextFloat()*2)-1;
    }
}
